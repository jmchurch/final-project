# README #

# G300 Final Project - Production 1 #

* In this project I took up the task of attempting to create an RTS game. Looking back at the goal I feel that this was too large an undertaking for the amount of time I had, one month.
* Although I didn't finish the game as much as I would have liked to for the deadline, I would like to keep working on it and continue to polish it.

## What were the three changes I made to the original tutorial? ##

* The original tutorial that I used to base this project off of was a video by Game Dev Guide about how to "correctly" make an RTS camera, https://www.youtube.com/watch?v=rnqF6S7PfFA&t=26s. I used his tutorial and modified it slightly, since it didn't stop the player from zooming in or out to extreme distances
## The Actual Assignment ##
### I made a ton of different features, but I think that the main three that I made completely by myself without help of tutorials were ###
* 1) Object Placement System
* 2) Object Selection System
* 3) Procedural Terrain Generation
### There are quite a few other features I had in the game, I will try to list them all off the top of my head ###
* enemy movement (used unity built in nav mesh)
* nav mesh update at runtime (used experimental unity nav mesh scripts https://github.com/Unity-Technologies/NavMeshComponents)  
* collectible collection
* Fog of War (took this entirely from https://andrewhungblog.wordpress.com/2018/06/23/implementing-fog-of-war-in-unity/, awesome blog btw)
* enemy spawning (I used what I learned from the fog of war implementation to use render textures to tell whether an area was completely dark and could spawn enemies)
* In terms of enemy spawning I learned object pooling from LlamAcademy and this video https://www.youtube.com/watch?v=0V99OBWmCHk, but what he was doing didn't really apply to my game type, so I had to find a different solution (which was using render textures to tell whether spawning should be possible or not), kind of annoying tutorials, but I learned so I guess that is all that matters
* I probably implemented some other stuff too

### What I Learned ###

The main thing that I learned was the pain that comes with working on a very large project. I figured out that if I want my workflow to be fast, I need to set up everything 
cleanly and in order so that I will be able to recognize where something should go when I decide to add a new mechanic. Related to a large project, I learned to use static 
variables more, which cut down on the complexity of some of my solutions to problems quite a bit.

### Stuff I didn't do ###

* All the good looking textures
* Textures.com
* Music
* Balance by Komiku, Music: https://www.chosic.com/free-music/all/
* Sound Effects
* Wind Realistic - https://www.imdb.com/name/nm10713602/
* Hello there - https://freesound.org/people/4barrelcarb/
* What? - https://freesound.org/people/szayelb/
* Yawning - https://freesound.org/people/aldenroth2/
* Voice Male Humming - Thinking - https://freesound.org/people/bolkmar/sounds/469603/ by bolkmar
* Folks from around here - https://freesound.org/people/alphahog/
* Man falling and yodelling - https://freesound.org/people/Robinhood76/
* Okay - https://freesound.org/people/nuncaconoci/