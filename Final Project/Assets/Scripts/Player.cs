using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    // Private Variables - Gold, Wood, Stone, Archers, Warriors, Villagers -
    private int gold;
    private int wood;
    private int stone;
    private bool objectDestroyed;
    private List<Vector3> coveredSpots; // this will most likely be the largest list for this class
    private List<Vector3> terrainSpots;
    private List<Vector3> treeSpots;
     
    private List<GameObject> archerList;
    private List<GameObject> warriorList;
    private List<GameObject> villagerList;
    private List<GameObject> houseOneList;
    private List<GameObject> barracksList;

    private GameObject townHall;
    
    // constructor will need a location orgument
    public Player()
    {
        // Defualt values for now (before balancing)
        // Gold - 500, Wood - 50, Stone - 0, archerList - empty, warriorList - empty, villagerList - 4
        gold = 500;
        wood = 50;
        stone = 0;
        coveredSpots = new List<Vector3>();
        terrainSpots = new List<Vector3>();
        treeSpots = new List<Vector3>();
        archerList = new List<GameObject>();
        warriorList = new List<GameObject>();
        villagerList = new List<GameObject>();
        houseOneList = new List<GameObject>();
        barracksList = new List<GameObject>();
    }
    
    // setters
    public void SetGold(int gold)
    {
        this.gold = gold;
    }

    public void SetWood(int wood)
    {
        this.wood = wood;
    }

    public void SetStone(int stone)
    {
        this.stone = stone;
    }

    public void SetTownHall(GameObject townHall)
    {
        this.townHall = townHall;
    }

    public void SetCoveredSpots(List<Vector3> pointList)
    {
        coveredSpots = pointList;
    }

    public void SetTerrainSpots(List<Vector3> pointList)
    {
        terrainSpots = pointList;
    }
    
    // list manipulators
    public void AddPoint(Vector3 point)
    {
        coveredSpots.Add(point);
    }

    public void AddTerrainPoint(Vector3 point)
    {
        terrainSpots.Add(point);
    }
    
    public void AddArcher(GameObject newArcher)
    {
        archerList.Add(newArcher);
    }

    public void AddWarrior(GameObject newWarrior)
    {
        warriorList.Add(newWarrior);
    }

    public void AddVillager(GameObject newVillager)
    {
        villagerList.Add(newVillager);
    }

    public void AddHouseOne(GameObject houseOne)
    {
        houseOneList.Add(houseOne);
    }

    public void AddBarracks(GameObject barracks)
    {
        barracksList.Add(barracks);
    }

    public void RemovePoints(List<Vector3> points)
    {
        foreach (Vector3 entry in points)
        {
            coveredSpots.Remove(entry);   
        }
    }

    public void RemovePoint(Vector3 point)
    {
        coveredSpots.Remove(point);
    }
    
    public void RemoveArcher(GameObject oldArcher)
    {
        archerList.Remove(oldArcher);
    }

    public void RemoveWarrior(GameObject oldWarrior)
    {
        warriorList.Remove(oldWarrior);
    }

    public void RemoveVillager(GameObject oldVillager)
    {
        villagerList.Remove(oldVillager);
    }

    public void RemoveHouseOne(GameObject houseOne)
    {
        houseOneList.Remove(houseOne);
    }

    public void RemoveBarracks(GameObject barracks)
    {
        barracksList.Remove(barracks);
    }
    
    // getters
    public int GetGold()
    {
        return gold;
    }

    public int GetWood()
    {
        return wood;
    }

    public int GetStone()
    {
        return stone;
    }

    public GameObject GetTownHall()
    {
        return townHall;
    }

    public List<Vector3> GetCoveredSpotsList()
    {
        return coveredSpots;
    }

    public List<Vector3> GetTerrainSpotsList()
    {
        return terrainSpots;
    }
    
    public List<GameObject> GetArcherList()
    {
        return archerList;
    }

    public List<GameObject> GetWarriorList()
    {
        return warriorList;
    }

    public List<GameObject> GetVillagerList()
    {
        return villagerList;
    }

    public List<GameObject> GetHouseOneList()
    {
        return houseOneList;
    }

    public List<GameObject> GetBarracksList()
    {
        return barracksList;
    } 

    public int GetArcherCount()
    {
        return archerList.Count;
    }

    public int GetWarriorCount()
    {
        return warriorList.Count;
    }

    public int GetVillagerCount()
    {
        return villagerList.Count;
    }

    public List<Transform> GetCharacterTransforms()
    {
        List<Transform> characterPositions = new List<Transform>();
        // add warriors first since they are most likely closer to the enemies
        for (int i = 0; i < warriorList.Count; i++)
        {
            if (warriorList[i].transform.childCount != 0) 
                characterPositions.Add(warriorList[i].transform.GetChild(0));
        }
        // add archers second since they are the second most likely to be in combat with the enemies
        for (int i = 0; i < archerList.Count; i++)
        {
            if (archerList[i].transform.childCount != 0)
                characterPositions.Add(archerList[i].transform.GetChild(0));
        }
        // add villagers last since they are the most helpless and least likely to be sent into battle against the enemies
        for (int i = 0; i < villagerList.Count; i++)
        {
            if (villagerList[i].transform.childCount != 0)
                characterPositions.Add(villagerList[i].transform.GetChild(0));
        }

        return characterPositions;
    }

    public List<Transform> GetAllDestroyableObjectTransforms()
    {
        List<Transform> objectPositions = new List<Transform>();
        
        // add warriors first since they are most likely closer to the enemies
        for (int i = warriorList.Count - 1; i >= 0; i--)
        {
            if (warriorList[i].transform.childCount != 0)
                objectPositions.Add(warriorList[i].transform.GetChild(0));
        }
        // add archers second since they are the second most likely to be in combat with the enemies
        for (int i = archerList.Count - 1; i >= 0; i--)
        { 
            if (archerList[i].transform.childCount != 0) 
                objectPositions.Add(archerList[i].transform.GetChild(0));
        }
        // add villagers last since they are the most helpless and least likely to be sent into battle against the enemies
        for (int i = villagerList.Count - 1; i >= 0; i--)
        {
            if (villagerList[i].transform.childCount != 0) 
                objectPositions.Add(villagerList[i].transform.GetChild(0));
        }
        // add houseOne buildings to the list
        for (int i = houseOneList.Count - 1; i >= 0; i--)
        {
            if (houseOneList[i].transform.childCount != 0) 
                objectPositions.Add(houseOneList[i].transform);
        }
        // add the barracks to the list
        for (int i = barracksList.Count - 1; i >= 0; i--)
        {
            if (barracksList[i].transform.childCount != 0) 
                objectPositions.Add(barracksList[i].transform);
        }
        
        return objectPositions;
    }
    
    public int GetHouseOneCount()
    {
        return houseOneList.Count;
    }

    public int GetBarracksCount()
    {
        return barracksList.Count;
    }

    public GameObject GetHouseOne(int index)
    {
        if (index < houseOneList.Count && index != -1)
        {
            return houseOneList[index];
        }

        return null;
    }
    
    public GameObject GetBarracks(int index)
    {
        if (index < barracksList.Count && index != -1)
        {
            return barracksList[index];
        }

        return null;
    }

    public int[] GetValuesList()
    {
        return new [] {gold, wood, stone, archerList.Count, warriorList.Count, villagerList.Count};
    }

    public void SetObjectDestroyed(bool a)
    {
        objectDestroyed = a;
    }
}
