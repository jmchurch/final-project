using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class VillagerBarracksCollision : MonoBehaviour
{
    private PlayerController player;
    private EnemySpawningDependingOnFogOfWar enemySpawningScript;
    private ObjectSelection objectSelection;
    private bool startedTraining = false;

    private void Awake()
    {
        player = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        enemySpawningScript =
            GameObject.Find("ProceduralEnemySpawner").GetComponent<EnemySpawningDependingOnFogOfWar>();
        objectSelection = GameObject.Find("SelectionController").GetComponent<ObjectSelection>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Villager") && !startedTraining)
        {
            startedTraining = true;
            player.GetPlayerOne().RemoveVillager(other.gameObject);
            objectSelection.RemoveSelection();
            StartCoroutine(DestroyCharacter(other));
            objectSelection.StartVillagerTraining();
            objectSelection.ChangeActiveStatusGiveVillagerSign(false);
            UIScript.DisableAllEditMenus();
            UIScript.EnableMainMenu();
        }
    }

    private IEnumerator DestroyCharacter(Collider other)
    {
        other.transform.DOMove(transform.position, .9f);
        yield return new WaitForSeconds(1);
        Destroy(other.gameObject);
        startedTraining = false;
        gameObject.SetActive(false);
    }
}
