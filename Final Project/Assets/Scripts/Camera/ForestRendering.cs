using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestRendering : MonoBehaviour
{
    public Transform cam;
    
    private bool _hasChildren = false;
    private bool _addingChildren = false;
    private List<GameObject> forestList;

    private void Start()
    {
        forestList = new List<GameObject>();
    }

    void Update()
    {
        if (transform.childCount > 0)
        {
            _hasChildren = true;
        }
        
        if (_hasChildren && !_addingChildren)
        {
            _addingChildren = true;
            for (int i = 0; i < transform.childCount; i++)
            {
                forestList.Add(transform.GetChild(i).gameObject);
            }
        }

        // check the distance between the camera and the positions of the forest,
        // if the camera is not within 750 units disable the GameObject
        // if the camera is within 750 units enable the GameObject
        if (forestList.Count != 0)
        {
            foreach (GameObject forest in forestList)
            {
                if (Vector3.Distance(forest.transform.position, cam.position) < 750)
                {
                    forest.SetActive(true);
                }
                else
                {
                    forest.SetActive(false);
                }
            }
        }
    }
}
