using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform zoomLimiter;
    public Transform cameraTransform;
    
    public float movementTime;
    public float normalSpeed;
    public float fastSpeed;
    public float rotationAmount;
    public float boundary; 
    public Vector3 zoomAmount;

    private float movementSpeed;
    private bool canZoom;
    private int screenWidth;
    private int screenHeight;
    
    private Vector3 newPosition;
    private Quaternion newRotation;
    public Vector3 newZoom;

    private Vector3 dragStartPosition;
    private Vector3 dragCurrentPosition;

    // Start is called before the first frame update
    void Start()
    {
        newPosition = transform.position;
        newRotation = transform.rotation;
        newZoom = cameraTransform.localPosition;
        // Initialize screen width and height
        screenWidth = Screen.width;
        screenHeight = Screen.height;
        // Initialize booleans
        canZoom = true;
    }

    // Update is called once per frame
    void Update()
    {
        // Camera movement method
        CheckSurroundings();
        HandleMovementInput();
        HandleMouseInput();
    }

    void HandleMouseInput()
    {
        // check if the mouse is within the boundary near the edge of the screen that will make the screen move
        if (Input.mousePosition.y > screenHeight - boundary){
            // mouse is in the top of the screen
            newPosition += (transform.forward * movementSpeed);
        }
        if (Input.mousePosition.y < 0 + boundary){
            // mouse is in the bottom of the screen
            newPosition += (transform.forward * -movementSpeed);
        }
        if (Input.mousePosition.x > screenWidth - boundary){
            // mouse is in the right of the screen
            newPosition += (transform.right * movementSpeed);
        }
        if (Input.mousePosition.x < 0 + boundary){
            // mouse is in the left of the screen
            newPosition += (transform.right * -movementSpeed);
        }
        
    /*
        if(Input.GetMouseButtonDown(0)){
            Plane plane = new Plane(Vector3.up, Vector3.zero);

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            float entry;

            if(plane.Raycast(ray, out entry)){
                dragStartPosition = ray.GetPoint(entry);
            }
        }
        // check if mouse is still held down
        if(Input.GetMouseButton(0)){
            Plane plane = new Plane(Vector3.up, Vector3.zero);

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            float entry;

            if(plane.Raycast(ray, out entry)){
                dragCurrentPosition = ray.GetPoint(entry);

                newPosition = transform.position + dragStartPosition - dragCurrentPosition;
            }
        }
        */

        // mouse scroll wheel is being moved away from player
        if(Input.mouseScrollDelta.y > 0 && canZoom)
        {
            newZoom += Input.mouseScrollDelta.y * zoomAmount;
        } 
        else if (Input.mouseScrollDelta.y < 0)
        {
            newZoom += Input.mouseScrollDelta.y * zoomAmount;
        }

        // clamp zoom at min Vector3(0,10.1700001f,-7.29999924f), max Vector3(0,146.169998,-143.300003)
        newZoom.y = Mathf.Clamp(newZoom.y, 10.1700001f, 146.17f);
        newZoom.z = Mathf.Clamp(newZoom.z, -143.3f,-35.2999992f);
    }
    void HandleMovementInput(){
        // determine movement speed depending on whether shift is held down
        if(Input.GetKey(KeyCode.LeftShift)){
            movementSpeed = fastSpeed;
        } else {
            movementSpeed = normalSpeed;
        }
        // have movement for both arrow keys and wasd
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)){
            newPosition += (transform.forward * movementSpeed);
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)){
            newPosition += (transform.forward * -movementSpeed);
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)){
            newPosition += (transform.right * movementSpeed);
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)){
            newPosition += (transform.right * -movementSpeed);
        }
        // rotate the camera if player is hitting Q or E
        if (Input.GetKey(KeyCode.Q)){
            newRotation *= Quaternion.Euler(Vector3.up * rotationAmount);
        }
        if (Input.GetKey(KeyCode.E)){
            newRotation *= Quaternion.Euler(Vector3.up * -rotationAmount);
        }

        if (Input.GetKey(KeyCode.R) && canZoom){
            // if a raycast isn't hit then zooming can continue
            newZoom += zoomAmount/10; // slow down the zoom /10 
        }
        
        if (Input.GetKey(KeyCode.F)){
            newZoom -= zoomAmount/10; // slow down the zoom /10
        }

        // lerp that smooths out movement and slows down depending on movementTime
        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * movementTime);
        // lerp that smooths out rotation and slows down depending on movementTime
        transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, Time.deltaTime * movementTime);
        // lerp that smooths the zoom of the camera and slows down depending on movementTime
        cameraTransform.localPosition = Vector3.Lerp(cameraTransform.localPosition, newZoom, Time.deltaTime * movementTime);

        // clamp zoom at min Vector3(0,10.1700001f,-7.29999924f), max Vector3(0,146.169998,-143.300003)
        newZoom.y = Mathf.Clamp(newZoom.y, 10.1700001f, 146.17f);
        newZoom.z = Mathf.Clamp(newZoom.z,-143.3f, -35.2999992f);
    }

    void CheckSurroundings()
    {
        // check if the camera is moving down into the ground
        RaycastHit hit;
        Vector3 cameraPos = zoomLimiter.position;
        if (Physics.Raycast(cameraPos, -Vector3.up, out hit, 10))
        {
            float difference = Mathf.Abs(cameraPos.y - hit.point.y);
            if (difference > 1)
            {
                StartCoroutine(ResetZoom());
                newZoom -= difference/10 * zoomAmount;
            }
        }
        
        /*
         * for all these positional checks the location of the camera needs to be influenced
         * in the opposite direction of where it is going.
         */
        // check if camera is moving forwards into the ground 
        if (Physics.Raycast(cameraPos, Vector3.forward, out hit, 10))
        {
            float difference = Mathf.Abs(cameraPos.y - hit.point.y);
            if (difference > 1)
            {
                StartCoroutine(ResetZoom());
                newPosition += (-Vector3.forward);
            }
        }
        
        // check if the camera is moving backwards into the ground
        if (Physics.Raycast(cameraPos, -Vector3.forward, out hit, 10))
        {
            float difference = Mathf.Abs(cameraPos.y - hit.point.y);
            if (difference > 1)
            {
                StartCoroutine(ResetZoom());
                newPosition += (Vector3.forward);
            }
        }
        
        // check if the camera is moving right into the ground
        if (Physics.Raycast(cameraPos, Vector3.right, out hit, 10))
        {
            float difference = Mathf.Abs(cameraPos.y - hit.point.y);
            if (difference > 1)
            {
                StartCoroutine(ResetZoom());
                newPosition += (-Vector3.right);
            }
        }
        
        // check if the camera is moving left into the ground
        if (Physics.Raycast(cameraPos, -Vector3.right, out hit, 10))
        {
            float difference = Mathf.Abs(cameraPos.y - hit.point.y);
            if (difference > 1)
            {
                StartCoroutine(ResetZoom());
                newPosition += (Vector3.right);
            }
        }
    }

    // turns off zooming ability, waits .5 seconds, turns zooming back on
    IEnumerator ResetZoom()
    {
        canZoom = false;
        yield return new WaitForSeconds(.5f);
        canZoom = true;
    }
}
