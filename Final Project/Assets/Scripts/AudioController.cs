using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class AudioController : MonoBehaviour
{
    public AudioSource[] audioSources;
    private static List<AudioSource> AudioSources = new List<AudioSource>();

    private void Awake()
    {
        for (int i = 0; i < audioSources.Length; i++)
        {
            AudioSources.Add(audioSources[i]);
        }
    }

    public static void PlayFolksFromAroundHere()
    {
        AudioSources[0].Play();
    }
    public static void PlayGuitarSong()
    {
        AudioSources[1].Play();
    }
    public static void PlayHelloThere()
    {
        AudioSources[2].Play();
    }
    public static void PlayHumming()
    {
        AudioSources[3].Play();
    }
    public static void PlayOkay()
    {
        AudioSources[4].Play();
    }
    public static void PlayWhat()
    {
        AudioSources[5].Play();
    }
    public static void PlayWind()
    {
        AudioSources[6].Play();
    }
    public static void PlayYawn()
    {
        AudioSources[7].Play();
    }
    public static void PlayYodellingAndFalling()
    {
        AudioSources[8].Play();
    }
    
    
    public static void PauseFolksFromAroundHere()
    {
        AudioSources[0].Pause();
    }
    public static void PauseGuitarSong()
    {
        AudioSources[1].Pause();
    }
    public static void PauseHelloThere()
    {
        AudioSources[2].Pause();
    }
    public static void PauseHumming()
    {
        AudioSources[3].Pause();
    }
    public static void PauseOkay()
    {
        AudioSources[4].Pause();
    }
    public static void PauseWhat()
    {
        AudioSources[5].Pause();
    }
    public static void PauseWind()
    {
        AudioSources[6].Pause();
    }
    public static void PauseYawn()
    {
        AudioSources[7].Pause();
    }
    public static void PauseYodellingAndFalling()
    {
        AudioSources[8].Pause();
    }

    public static void PlayRandomGreeting()
    {
        int rand = UnityEngine.Random.Range(2, 6);
        if (rand == 4 || rand == 5)
            rand = 3;
        AudioSources[rand].Play();
    }
}
