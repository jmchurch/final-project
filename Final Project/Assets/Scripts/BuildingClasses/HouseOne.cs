using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseOne : MonoBehaviour
{
    // Important variables - spots covered, health -

    private int health = 750;
    private int dailyIncome = 50;
    public List<Vector3> coveredSpots;

    private void Awake()
    {
        coveredSpots = new List<Vector3>();
    }

    public void SetSpots(List<Vector3> coveredSpots)
    {
        this.coveredSpots = new List<Vector3>(); // make sure that the list is empty
        foreach (Vector3 entry in coveredSpots)
        {
            this.coveredSpots.Add(entry);
        }
    }
    
    public List<Vector3> GetSpots()
    {
        return coveredSpots;
    }

    public int GetDailyIncome()
    {
        return dailyIncome;
    }
}
