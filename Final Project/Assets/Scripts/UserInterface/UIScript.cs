using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIScript : MonoBehaviour
{
    public GameObject mainCanvas;
    private static GameObject mainUICanvas;
    public Text[] textBoxList;
    private static GameObject mainMenu;
    private static List<GameObject> storeList;
    private static List<GameObject> editList;
    public TextMeshProUGUI clockText;
    
    //public TextMeshProUGUI clockText;
    public PlayerController playerInfo;
    
    [SerializeField] private int clockSpeed = 50;
    
    private Player playerOne;
    private int[] valueList;
    private int days = 1;
    private bool oneDayPassed;

    private void Start()
    {
        mainUICanvas = mainCanvas;
        mainMenu = mainUICanvas.transform.GetChild(2).gameObject;
        storeList = new List<GameObject>();
        storeList.Add(mainUICanvas.transform.GetChild(3).gameObject);
        storeList.Add(mainUICanvas.transform.GetChild(4).gameObject);
        editList = new List<GameObject>();
        editList.Add(mainUICanvas.transform.GetChild(5).gameObject);
        editList.Add(mainUICanvas.transform.GetChild(6).gameObject);
        editList.Add(mainUICanvas.transform.GetChild(7).gameObject);
    }

    private void Awake()
    {
        Time.timeScale = 0;
        oneDayPassed = false;
        valueList = new int[6];
        playerOne = playerInfo.GetPlayerOne();
    }

    private void Update()
    {
        if (Time.timeScale != 0)
        {
            UpdateUI();
            UpdateClock();
        }
    }

    private void UpdateUI()
    {
        for (int i = 0; i < valueList.Length; i++)
        {
            textBoxList[i].text = playerOne.GetValuesList()[i].ToString();
        }
    }
    
    private void UpdateClock()
    {
        float t = Time.timeSinceLevelLoad * clockSpeed; // time since scene loaded
        t /= 60; // divide current time y 60 to get minutes
        int minutes = (int)(t % 60); //return the remainder of the minutes divide by 60 as an int
        t /= 60; // divide current time to get hours
        int hours = (int) (t % 24);
        SetDays(hours, minutes);
        clockText.text = string.Format("Day: {0}, {1}:{2}", days.ToString(), hours.ToString("00"), minutes.ToString("00"));
    }
    
    private void SetDays(int hours, int minutes)
    {
        if (hours == 0 && !oneDayPassed)
        {
            oneDayPassed = true;
        }
        if (hours == 23 && minutes == 59 && oneDayPassed)
        {
            oneDayPassed = false;
            days++;
            // add resources from the player controller
            playerInfo.AddDailyResources();
        }
    }

    public static void EnableMainUICanvas()
    {
        mainUICanvas.SetActive(true);
    }
    
    public static void EnableMainMenu()
    {
        mainMenu.SetActive(true);
    }

    public static void EnableEditMenu(int index)
    {
        editList[index].SetActive(true);
    }

    public static void EnableStoreMenu(int index)
    {
        editList[index].SetActive(true);
    }

    public static void DisableMainUICanvas()
    {
        mainUICanvas.SetActive(false);
    }
    
    public static void DisableMainMenu()
    {
        mainMenu.SetActive(false);
    }
    
    public static void DisableEditMenu(int index)
    {
        editList[index].SetActive(false);
    }

    public static void DisableAllEditMenus()
    {
        foreach (GameObject menu in editList)
        {
            menu.SetActive(false);
        }
    }

    public static void DisableStoreMenu(int index)
    {
        storeList[index].SetActive(false);
    }

    public static void DisableAllStoreMenus()
    {
        foreach (GameObject menu in storeList)
        {
            menu.SetActive(false);
        }
    }

    public static void DisableVillagerInputField()
    {
        GameObject menu = GetEditMenuList()[2];
        Transform inputField = menu.transform.GetChild(1);
        inputField.gameObject.SetActive(false);
    }

    public static void EnableArcherWarriorButtons()
    {
        GameObject menu = GetEditMenuList()[1];
        menu.transform.GetChild(1).gameObject.SetActive(true);
        menu.transform.GetChild(2).gameObject.SetActive(true);
        menu.transform.GetChild(3).gameObject.SetActive(true);
    }

    public static void DisableArcherWarriorButtons()
    {
        GameObject menu = GetEditMenuList()[1];
        menu.transform.GetChild(1).gameObject.SetActive(false);
        menu.transform.GetChild(2).gameObject.SetActive(false);
        menu.transform.GetChild(3).gameObject.SetActive(false);
    }

    public static List<GameObject> GetStoreMenuList()
    {
        return storeList;
    }

    public static List<GameObject> GetEditMenuList()
    {
        return editList;
    }
}
