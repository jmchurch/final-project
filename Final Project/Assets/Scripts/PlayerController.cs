using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Initialize player class
    [SerializeField]
    private Player playerOne;
    
    // character list
    public GameObject[] characters;

        // Start is called before the first frame update
    void Awake()
    {
        playerOne = new Player();
    }

    public Player GetPlayerOne()
    {
        return playerOne;
    }

    public void AddDailyResources()
    {
        List<GameObject> houseOneList = playerOne.GetHouseOneList();
        foreach (GameObject entry in houseOneList)
        {
            int goldAmount = entry.GetComponent<HouseOne>().GetDailyIncome();
            playerOne.SetGold(playerOne.GetGold() + goldAmount);   
        }
    }

    public void RemoveHouseOne(GameObject house)
    {
        playerOne.SetGold(playerOne.GetGold() + 50);
        playerOne.SetWood(playerOne.GetWood() + 5);
        // remove house from the viewport
        Destroy(house);
        // remove the points the house was covering from the player's covered spots list
        playerOne.RemovePoints(house.GetComponent<HouseOne>().GetSpots());
        // remove the house from the list that is contained in the PlayerController
        playerOne.RemoveHouseOne(house);
    }

    public void RemoveBarracks(GameObject barracks)
    {
        playerOne.SetGold(playerOne.GetGold() + 150);
        playerOne.SetWood(playerOne.GetWood() + 15);
        playerOne.SetStone(playerOne.GetStone() + 5);
        // remove barracks from the viewport
        Destroy(barracks);
        // remove the points that the barracks was covering from the player's covered spots list
        playerOne.RemovePoints(barracks.GetComponent<Barracks>().GetSpots());
        // remove the house from the list that is contained in the PlayerController
        playerOne.RemoveBarracks(barracks);
    }

    public void InstantiateVillager(Vector3 position)
    {
        GameObject villager = Instantiate(characters[0], position, Quaternion.identity, playerOne.GetTownHall().transform.GetChild(1));
        playerOne.AddVillager(villager);
    }
    
    public void InstantiateArcher(Vector3 position)
    {
        GameObject villager = Instantiate(characters[1], position, Quaternion.identity, playerOne.GetTownHall().transform.GetChild(2));
        playerOne.AddVillager(villager);
    }
    
    public void InstantiateWarrior(Vector3 position)
    {
        GameObject villager = Instantiate(characters[2], position, Quaternion.identity, playerOne.GetTownHall().transform.GetChild(3));
        playerOne.AddVillager(villager);
    }
}
