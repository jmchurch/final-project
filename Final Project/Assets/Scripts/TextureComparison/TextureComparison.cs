using System;
using UnityEditor;
using UnityEngine;

public class TextureComparison : MonoBehaviour
{
    // public static Hash128 GetHash128Value (SerializedProperty property)
    // {
    //     if (property.type != "Hash128")
    //     {
    //         throw new Exception("SerializedProperty does not represent a Hash128 struct.");
    //     }
    //
    //     var bytes = new byte[4][];
    //
    //     for (var i = 0; i < 4; i++)
    //     {
    //         bytes[i] = new byte[4];
    //
    //         for (var j = 0; j < 4; j++)
    //         {
    //             property.Next(true);
    //             bytes[i][j] = (byte) property.intValue;
    //         }
    //     }
    //
    //     var hash = new Hash128(
    //         BitConverter.ToUInt32(bytes[0], 0),
    //         BitConverter.ToUInt32(bytes[1], 0),
    //         BitConverter.ToUInt32(bytes[2], 0),
    //         BitConverter.ToUInt32(bytes[3], 0)
    //     );
    //
    //     return hash;
    // }
    
    public static Texture2D RenderTextureToTexture2D(RenderTexture texture)
    {
        RenderTexture.active = texture;
        Texture2D renderTexture2D = new Texture2D(texture.width,texture.height);
        renderTexture2D.ReadPixels(new Rect(0,0,texture.width,texture.height), 0,0);
        renderTexture2D.Apply();

        return renderTexture2D;
    }

    public static Color[] ManipulatePixels(Color[] oldColorList, float newRedValue, float newGreenValue, float newBlueValue, float newAlphaValue)
    {
        for (int i = 0; i < oldColorList.Length; i++)
        {
            oldColorList[i] = new Color(newRedValue, newGreenValue, newBlueValue, newAlphaValue);
        }

        return oldColorList;
    }
}
