using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Grid : MonoBehaviour
{
    [SerializeField] private float size = 1f;

    public Vector3 GetNearestPointOnGrid(Vector3 position, float yOffset)
    {
        position -= transform.position;

        int xCount = Mathf.RoundToInt(position.x / size);
        int yCount = Mathf.RoundToInt(position.y / size );
        int zCount = Mathf.RoundToInt(position.z / size);

        Vector3 result = new Vector3(
            (float) xCount * size,
            (float) yCount * size + yOffset,
            (float) zCount * size);
        
        result += transform.position;
        return result;
    }

    public float GetSize()
    {
        return size;
    }
}
