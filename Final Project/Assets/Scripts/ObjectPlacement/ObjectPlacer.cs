using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlacer : MonoBehaviour
{
    public GameObject[] placeableGameObjectList;
    public PlayerController player;
    public ObjectSelection objectSelection;
    public Camera cam;
    public Camera overheadCam;
    public Grid grid;

    private GameObject placedObject;
    private int currObject = 0;

    private bool coveringCharacter = false;
    private bool buttonClicked = false;
    private bool inStartSection = true;
    [SerializeField] private bool objectFinalized;

    private void Awake()
    {
        // allows selection
        objectFinalized = true;
        cam = Camera.main;
    }

    private void Update()
    {
        if (buttonClicked)
        {
            RaycastHit hitInfo;
            Ray ray;
            if (!overheadCam.isActiveAndEnabled && !inStartSection)
            {
                ray = cam.ScreenPointToRay(Input.mousePosition);
            }
            else
            {
                ray = overheadCam.ScreenPointToRay(Input.mousePosition);
            }
            

            if (Physics.Raycast(ray, out hitInfo))
            {
                if (hitInfo.collider.CompareTag("Ground") && placedObject != null)
                {
                    placedObject.transform.position = grid.GetNearestPointOnGrid(hitInfo.point, 4f);
                }
            }
        }

        if (Input.GetMouseButtonDown(0) && buttonClicked)
        {
            objectFinalized = false; // reset finalized boolean in case back button was not pressed
            Vector3 placementPoint = new Vector3();
            if (placedObject != null)
            {
                placementPoint = placedObject.transform.position;
            }
            else
            {
                placementPoint = Vector3.zero;
            }

            float currSize = grid.GetSize();
            // check if the squares the object is covering is in the list of covered squares and if it is covering a character
            if (CheckSquares(placementPoint, currSize) && !coveringCharacter)
            {
                // add the squares that the object is covering to the list of covered squares
                AddSquares(placementPoint, currSize);
                // remove resources from the player
                RemoveResources();
                // add the rigidbody and box collider components
                AddComponents();
                buttonClicked = false;
            }
            /*
             * bool objectFinalized is currently false, so if the back button is pressed in the same frame the house should be deleted
             */
            if (Time.timeScale != 0)
            {
                StartCoroutine(FinalizePlacement());
            }
            else
            {
                objectSelection.ZeroBoxCorners();
                objectFinalized = true;
            }
        }
    }

    // Set currObject to 0
    public void InstantiateHouseObject()
    {
        Player playerOne = player.GetPlayerOne();
        if (playerOne.GetGold() >= 150 && playerOne.GetWood() >= 10)
        {
            Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                if (hitInfo.transform.root.CompareTag("Terrain"))
                {
                    // if the center of the player's camera is currently over terrain, don't spawn a building
                    return;
                }
                playerOne.AddHouseOne(Instantiate(placeableGameObjectList[0],
                    grid.GetNearestPointOnGrid(hitInfo.point, 4f), Quaternion.identity, playerOne.GetTownHall().transform.GetChild(4)));
                placedObject = playerOne.GetHouseOne(playerOne.GetHouseOneCount() - 1);
                placedObject.transform.eulerAngles = new Vector3(-90, 0, 0);
                // add a BoxCollider to the object that will check if the house is over a character
                placedObject.AddComponent<BoxCollider>().isTrigger = true;
                currObject = 0;
                buttonClicked = true;
            }
        }
    }
    
    // Set currObject to 1
    public void InstantiateBarracksObject()
    {
        Player playerOne = player.GetPlayerOne();
        if (playerOne.GetGold() >= 500 && playerOne.GetWood() >= 50 && playerOne.GetStone() >= 15)
        {
            Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                if (hitInfo.transform.root.CompareTag("Terrain"))
                {
                    // if the center of the player's camera is currently over terrain, don't spawn a building
                    return;
                }
                playerOne.AddBarracks(Instantiate(placeableGameObjectList[1], grid.GetNearestPointOnGrid(hitInfo.point, 4f), 
                    Quaternion.identity, playerOne.GetTownHall().transform.GetChild(5)));
                placedObject = playerOne.GetBarracks(playerOne.GetBarracksCount() - 1);
                // add a BoxCollider to the object that will check if the barracks is over a character
                placedObject.transform.GetChild(0).gameObject.AddComponent<BoxCollider>().isTrigger = true;
                currObject = 1;
                buttonClicked = true;
            }
        }
    }
    
    public GameObject InstantiateTownHallObject()
    {
        Player playerOne = player.GetPlayerOne();
        
        Ray ray = overheadCam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo))
        {
            if (hitInfo.transform.root.CompareTag("Ground"))
            {
                // if the center of the player's camera is currently over terrain, don't spawn a building
                playerOne.SetTownHall(Instantiate(placeableGameObjectList[2], grid.GetNearestPointOnGrid(hitInfo.point, 4f), Quaternion.identity));
                placedObject = playerOne.GetTownHall();
                // add a BoxCollider to the object that will check if the barracks is over a character
                placedObject.AddComponent<BoxCollider>().isTrigger = true;
                currObject = 2;
                buttonClicked = true;
                return placedObject;
            }
        }

        ray = overheadCam.ViewportPointToRay(new Vector3(.25f, .25f, 0));
        if (Physics.Raycast(ray, out hitInfo))
        {
            if (hitInfo.transform.root.CompareTag("Ground"))
            {
                // if the center of the player's camera is currently over terrain, don't spawn a building
                playerOne.SetTownHall(Instantiate(placeableGameObjectList[2], grid.GetNearestPointOnGrid(hitInfo.point, 4f), Quaternion.identity));
                placedObject = playerOne.GetTownHall();
                // add a BoxCollider to the object that will check if the barracks is over a character
                placedObject.AddComponent<BoxCollider>().isTrigger = true;
                currObject = 2;
                buttonClicked = true;
                return placedObject;
            }
        }
        ray = overheadCam.ViewportPointToRay(new Vector3(.25f, .75f, 0));
        if (Physics.Raycast(ray, out hitInfo))
        {
            if (hitInfo.transform.root.CompareTag("Ground"))
            {
                // if the center of the player's camera is currently over terrain, don't spawn a building
                playerOne.SetTownHall(Instantiate(placeableGameObjectList[2], grid.GetNearestPointOnGrid(hitInfo.point, 4f), Quaternion.identity));
                placedObject = playerOne.GetTownHall();
                // add a BoxCollider to the object that will check if the barracks is over a character
                placedObject.AddComponent<BoxCollider>().isTrigger = true;
                currObject = 2;
                buttonClicked = true;
                return placedObject;
            }
        }
        ray = overheadCam.ViewportPointToRay(new Vector3(.75f, .25f, 0));
        if (Physics.Raycast(ray, out hitInfo))
        {
            if (hitInfo.transform.root.CompareTag("Ground"))
            {
                // if the center of the player's camera is currently over terrain, don't spawn a building
                playerOne.SetTownHall(Instantiate(placeableGameObjectList[2], grid.GetNearestPointOnGrid(hitInfo.point, 4f), Quaternion.identity));
                placedObject = playerOne.GetTownHall();
                // add a BoxCollider to the object that will check if the barracks is over a character
                placedObject.AddComponent<BoxCollider>().isTrigger = true;
                currObject = 2;
                buttonClicked = true;
                return placedObject;
            }
        }
        ray = overheadCam.ViewportPointToRay(new Vector3(.75f, .75f, 0));
        if (Physics.Raycast(ray, out hitInfo))
        {
            // if the center of the player's camera is currently over terrain, don't spawn a building
            playerOne.SetTownHall(Instantiate(placeableGameObjectList[2], grid.GetNearestPointOnGrid(hitInfo.point, 4f), Quaternion.identity));
            placedObject = playerOne.GetTownHall();
            // add a BoxCollider to the object that will check if the barracks is over a character
            placedObject.AddComponent<BoxCollider>().isTrigger = true;
            currObject = 2;
            buttonClicked = true;
            return placedObject;
        }

        return null;
    }

    // check if the spot that the player is currently trying to place an object on is being used
    // by another building
    public bool CheckSquares(Vector3 placementPoint, float currSize)
    {
        Player playerOne = player.GetPlayerOne();
        List<Vector3> coveredSpots = playerOne.GetCoveredSpotsList();
        foreach (Vector3 terrainPoint in playerOne.GetTerrainSpotsList())
        {
            if (Vector3.Distance(placementPoint, terrainPoint) < 40)
            {
                // if the placement point is within 20 units of a terrainBlocks center return false 
                return false;
            }    
        }
        if (currObject == 0)
        {
            // check if any of the points, one up, one diagonal, one right are already in the usedSquares list
            if (!coveredSpots.Contains(placementPoint) &&
                !coveredSpots.Contains(placementPoint + new Vector3(currSize, 0f, 0f)) &&
                !coveredSpots.Contains(placementPoint + new Vector3(0f, 0f, currSize)) &&
                !coveredSpots.Contains(placementPoint + new Vector3(currSize, 0f, currSize)))
            {
                return true;
            }

            return false;
        } 
        if (currObject == 1 || currObject == 2)
        {
           if (!coveredSpots.Contains(placementPoint) &&
               !coveredSpots.Contains(placementPoint + new Vector3(currSize, 0f, 0f)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(0f, 0f, currSize)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(currSize, 0f, currSize)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(currSize * 2, 0f, 0f)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(0f, 0f, currSize * 2)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(currSize * 2, 0f, currSize * 2)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(-currSize, 0f, -currSize)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(-currSize, 0f, 0f)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(0f,0f,-currSize)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(currSize * 2,0f,-currSize)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(-currSize,0f,currSize*2)) &&
               !coveredSpots.Contains(placementPoint + new Vector3(-currSize,0f,-currSize)))
           {
               return true;
           }

           return false;
        }
        // won't be called
        return true;
    }

    // Add the covered spots to the player's covered spots list
    public void AddSquares(Vector3 placementPoint, float currSize)
    {
        Player playerOne = player.GetPlayerOne();
        List<Vector3> coveredSpots = playerOne.GetCoveredSpotsList();
        List<Vector3> tempList = new List<Vector3>();
        if (currObject == 0)
        {
            // add the points, one up, one diagonal, one right to the usedSquares list
            tempList.Add(placementPoint);
            tempList.Add(placementPoint + new Vector3(currSize, 0f, 0f));
            tempList.Add(placementPoint + new Vector3(0f, 0f, currSize));
            tempList.Add(placementPoint + new Vector3(currSize, 0f, currSize));
            // set the spots for the object that was initialized
            playerOne.GetHouseOne(playerOne.GetHouseOneCount() - 1).GetComponent<HouseOne>().SetSpots(tempList);
            foreach (Vector3 entry in tempList)
            {
                coveredSpots.Add(entry); // add the to the list that remembers locations for placement
            }
        }
        else if (currObject == 1 || currObject == 2)
        {
            tempList.Add(placementPoint);
            tempList.Add(placementPoint + new Vector3(currSize, 0f, 0f));
            tempList.Add(placementPoint + new Vector3(0f, 0f, currSize));
            tempList.Add(placementPoint + new Vector3(currSize, 0f, currSize));
            tempList.Add(placementPoint + new Vector3(currSize * 2, 0f, 0f));
            tempList.Add(placementPoint + new Vector3(0f, 0f, currSize * 2));
            tempList.Add(placementPoint + new Vector3(currSize * 2, 0f, currSize * 2));
            // behind the click point
            tempList.Add(placementPoint + new Vector3(-currSize, 0f, -currSize));
            tempList.Add(placementPoint + new Vector3(-currSize, 0f, 0f));
            tempList.Add(placementPoint + new Vector3(0f,0f,-currSize));
            tempList.Add(placementPoint + new Vector3(currSize * 2,0f,-currSize));
            tempList.Add(placementPoint + new Vector3(-currSize,0f,currSize*2));
            tempList.Add(placementPoint + new Vector3(-currSize,0f,-currSize));
            // set the spots for the object that was initialized
            if (currObject == 1)
            {
                // hold the spots in the object personally
                playerOne.GetBarracks(playerOne.GetBarracksCount() - 1).GetComponent<Barracks>().SetSpots(tempList);
            }
            // add the spots to the list of coveredSpots holding all the spots
            foreach (Vector3 entry in tempList)
            {
                coveredSpots.Add(entry);
            }
        }
    }

    // Adds rigidbody and box collider components after being placed to the
    // buildings so that they do not influence characters while being placed
    public void AddComponents()
    {
        Player playerOne = player.GetPlayerOne();
        if (currObject == 0) // current object is a tier one house
        {
            GameObject currHouse = playerOne.GetHouseOne(playerOne.GetHouseOneCount() - 1);
            Transform currHouseTransform = currHouse.transform;
            currHouseTransform.position = currHouseTransform.position + new Vector3(0, -.25f, 0);
            // remove the trigger BoxCollider
            Destroy(currHouse.GetComponent<BoxCollider>());
            // add a kinematic rigidbody to the house
            currHouse.AddComponent<Rigidbody>().isKinematic = true;
            // add a box collider to the house
            currHouse.AddComponent<BoxCollider>();
            currHouse.isStatic = true;
            // allow the nav mesh to be updated in real time
            currHouse.AddComponent<NavMeshSourceTag>();
            // enable the FOV model
            currHouseTransform.GetChild(0).gameObject.SetActive(true);
            player.InstantiateVillager(currHouseTransform.position + new Vector3(0,0,10));
        }
        else if (currObject == 1) // current object is a barracks
        {
            GameObject currBarracks = playerOne.GetBarracks(playerOne.GetBarracksCount() - 1);
            // remove the trigger BoxCollider
            Destroy(currBarracks.transform.GetChild(0).gameObject.GetComponent<BoxCollider>());
            // add a kinematic rigidbody to the barracks
            currBarracks.transform.GetChild(0).gameObject.AddComponent<Rigidbody>().isKinematic = true;
            // add a box collider to the barracks
            currBarracks.transform.GetChild(0).gameObject.AddComponent<BoxCollider>();
            currBarracks.transform.GetChild(0).gameObject.isStatic = true;
            // allow the nav mesh to be updated in real time
            currBarracks.transform.GetChild(0).gameObject.AddComponent<NavMeshSourceTag>();
            // enable the FOV model
            currBarracks.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
        }
        else if (currObject == 2) // current object is the townHall
        {
            GameObject currTownHall = playerOne.GetTownHall();
            // remove the tirgger BoxCollider
            Destroy(currTownHall.GetComponent<BoxCollider>());
            // add a kinematic rigidbody to the barracks
            currTownHall.AddComponent<Rigidbody>().isKinematic = true;
            // add a box collider to the barracks
            currTownHall.AddComponent<BoxCollider>();
            currTownHall.isStatic = true;
            // allow the nav mesh to be updated in real time
            currTownHall.AddComponent<NavMeshSourceTag>();
            // this is special for the townHall
            // need to: unpause game, tween between overhead camera and the RTS camera, enable the UI Canvas, Spawn the villagers around the TownHall
            // activate the FOV gameObject
            currTownHall.transform.GetChild(8).gameObject.SetActive(true);
            inStartSection = GameObject.Find("Terrain").GetComponent<TerrainSpawning>().TownHallPlaced(currTownHall.transform.position, overheadCam);
            
        }
    }

    // Is called when the back button in the menu system is played
    // Meant to destroy object if it had not been placed before hitting
    // the back button.
    public void BackButtonPressed(int selection)
    {
        currObject = selection;
        if (!objectFinalized)
        {
            // Right now this is only removing from the HouseList
            Player playerOne = player.GetPlayerOne();
            // remove the gameObject from the game
            if (currObject == 0)
            {
                GameObject house = playerOne.GetHouseOne(playerOne.GetHouseOneCount() - 1);
                // check if house is null
                if (house != null)
                {
                    // add the resources back to the player - 150 GOLD, 10 WOOD -
                    playerOne.SetGold(playerOne.GetGold() + 150);
                    playerOne.SetWood(playerOne.GetWood() + 10);
                    // remove house from the viewport
                    Destroy(house);
                    // remove the points the house was covering from the player's covered spots list
                    playerOne.RemovePoints(house.GetComponent<HouseOne>().GetSpots());
                    // remove the house from the list that is contained in the PlayerController
                    playerOne.RemoveHouseOne(house);
                }
            }
            else if (currObject == 1)
            {
                GameObject barracks = playerOne.GetBarracks(playerOne.GetBarracksCount() - 1);
                // check if barracks is null
                if (barracks != null)
                {
                
                    playerOne.SetGold(playerOne.GetGold() + 500);
                    playerOne.SetWood(playerOne.GetWood() + 50);
                    playerOne.SetStone(playerOne.GetStone() + 15);
                    // remove barracks from the viewport
                    Destroy(barracks);
                    // remove the points that the barracks was covering from the player's covered spots list
                    playerOne.RemovePoints(barracks.GetComponent<Barracks>().GetSpots());
                    // remove the house from the list that is contained in the PlayerController
                    playerOne.RemoveBarracks(barracks);
                }
            }

            // set backButtonClicked to false
            // reset the value of the placedObject to null
            placedObject = null;
        }
    }

    public void RemoveResources()
    {
        Player playerOne = player.GetPlayerOne();
        if (currObject == 0) // current object is a houseOne
        {
            // Remove materials - 150 GOLD, 10 WOOD -
            playerOne.SetGold(playerOne.GetGold() - 150);
            playerOne.SetWood(playerOne.GetWood() - 10);
        }
        else if (currObject == 1) //  current object is a Barracks
        {
            // Remove materials - 500 GOLD, 50 WOOD, 15 STONE -
            playerOne.SetGold(playerOne.GetGold() - 500);
            playerOne.SetWood(playerOne.GetWood() - 50);
            playerOne.SetStone(playerOne.GetStone() - 15);
        }
    }

    public void SetFinalizePlacement(bool a)
    {
        objectFinalized = a;
    }

    public bool GetObjectFinalized()
    {
        return objectFinalized;
    }
    
    public void ResetFinalized()
    {
        objectFinalized = false;
    }

    public void SetCoveringCharacter(bool a)
    {
        coveringCharacter = a;
    }

    public void SetRTSCam(Camera rtsCamera)
    {
        cam = rtsCamera;
    }

    public void SetOverheadCam(Camera overheadCamera)
    {
        overheadCam = overheadCamera;
    }

    IEnumerator FinalizePlacement()
    {
        yield return new WaitForSeconds(.2f);
        // reset the values of the BoxCollider
        objectSelection.ZeroBoxCorners();
        objectFinalized = true;
    }
}
