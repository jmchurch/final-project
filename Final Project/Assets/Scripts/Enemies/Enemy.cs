using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : PoolableObject
{
    public EnemyMovement movement;
    public NavMeshAgent agent;
    public int health = 100;
    public int damage = 10;
    public int attackSpeed = 3;


    private void Update()
    {
        if (health <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    public override void OnDisable()
    {
        base.OnDisable();
        agent.enabled = false;
    }

    public void SetHealth(int health)
    {
        this.health = health;
    }

    public void SetDamage(int damage)
    {
        this.damage = damage;
    }

    public void RemoveHealth(int damageDealt)
    {
        health -= damageDealt;
    }

    public void SetSpeed(float speed)
    {
        agent.speed = speed;
    }
    
    // getters
    public int GetDamage()
    {
        return damage;
    }

    public int GetHealth()
    {
        return health;
    }

    public int GetAttackSpeed()
    {
        return attackSpeed;
    }

}
