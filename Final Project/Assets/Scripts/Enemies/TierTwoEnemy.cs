using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TierTwoEnemy : MonoBehaviour
{
    private Enemy currEnemyScript;

    private void Awake()
    {
        currEnemyScript = GetComponent<Enemy>();
        
        currEnemyScript.SetDamage(30);
        currEnemyScript.SetHealth(300);
        currEnemyScript.SetSpeed(4.25f);
    }
}
