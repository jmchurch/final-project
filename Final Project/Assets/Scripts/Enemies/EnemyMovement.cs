using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.PlayerLoop;

public class EnemyMovement : MonoBehaviour
{
    public float updateRate = 0.1f;
    private NavMeshAgent agent;
    private Coroutine followCoroutine;
    private Transform player;
    private PlayerController playerController;

    private void Awake()
    {
        playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        agent = GetComponent<NavMeshAgent>();
    }

    public void StartChasing()
    {
        if (followCoroutine == null)
        {
            followCoroutine = StartCoroutine(nameof(FollowTarget));
        }
        else
        {
            Debug.LogWarning("Called StartChasing on enemy that is already chasing, likely a bug in some calling class");
        }
    }

    private IEnumerator FollowTarget()
    {
        WaitForSeconds wait = new WaitForSeconds(updateRate);

        // start the recursive coroutine that will continuously update the player
        StartCoroutine(nameof(UpdatePlayer));

        // for now it will not matter the distance between the target character
        
        while (enabled)
        {
            if (player != null && Vector3.Distance(player.position, transform.position) <= 50)
            {
                agent.SetDestination(player.position);
            }
            else
            {
                // stop the nav mesh agent and remove the path it was moving along
                player = null;
                agent.isStopped = true;
                agent.ResetPath();
            }
            yield return wait;
        }
    }

    private IEnumerator UpdatePlayer()
    {
        WaitForSeconds wait = new WaitForSeconds(2);
        
        Player playerOne = playerController.GetPlayerOne();
        // find the current character to follow, then start a coroutine that will update the 
        List<Transform> destroyableObjectTransforms = playerOne.GetAllDestroyableObjectTransforms();

        // if there are characters left, then check the distances between them and the enemy
        if (destroyableObjectTransforms != null && destroyableObjectTransforms.Count != 0)
        {
            int transformIndex = 0;
            float shortestDistance = float.MaxValue;

            for (int i = 0; i < destroyableObjectTransforms.Count; i++)
            {
                float distance = Vector3.Distance(transform.position, destroyableObjectTransforms[i].position);
                if (distance < shortestDistance)
                {
                    // set the distance again
                    shortestDistance = distance;
                    // update the transformIndex
                    transformIndex = i;
                }
            }

            // make sure that the distance is in range
            if (shortestDistance < 25)
            {
                player = destroyableObjectTransforms[transformIndex];
            }
            else
            {
                // if the enemies are within x units of the townHall, but not within y units of another object, then they
                // will target the townHall
                Transform townHallTransform = GameObject.FindGameObjectWithTag("TownHall").transform;
                if (Vector3.Distance(townHallTransform.position, transform.position) <= 50)
                {
                    player = townHallTransform;
                }
            }
        }
        yield return wait;
        StartCoroutine(nameof(UpdatePlayer));
    }


    // find out what type of character the player is, and determine whether their health is below zero
    private bool PlayerHealthBelowZero()
    {
        if (player == null)
        {
            return false;
        }
        
        if (player.CompareTag("Villager"))
        {
            Villager currCharacterScript = player.GetComponent<Villager>();
            if (currCharacterScript.GetHealth() <= 0)
                return true;
        }
        
        if (player.CompareTag("Archer"))
        {
            Archer currCharacterScript = player.GetComponent<Archer>();
            if (currCharacterScript.GetHealth() <= 0)
                return true;
        }
        
        if (player.CompareTag("Warrior"))
        {
            Warrior currCharacterScript = player.GetComponent<Warrior>();
            if (currCharacterScript.GetHealth() <= 0)
                return true;
        }

        return false;
    }

    // setters
    public void SetPlayer(Transform newPlayer)
    {
        player = newPlayer;
    }
}
