using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    private Enemy enemyScript;
    private EnemyMovement enemyMovementScript;
    private List<Collider> attackingVillagerColliders = new List<Collider>();
    private List<Collider> attackingArcherColliders = new List<Collider>();
    private List<Collider> attackingWarriorColliders = new List<Collider>();

    private Collider attackedVillager;
    private Collider attackedArcher;
    private Collider attackedWarrior;

    private bool attackingObject;

    private void Awake()
    {
        attackingObject = false;
        attackedVillager = null;
        attackedArcher = null;
        attackedWarrior = null;
        enemyScript = transform.parent.GetComponent<Enemy>();
        enemyMovementScript = transform.parent.GetComponent<EnemyMovement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Villager"))
        {
            if (attackingObject)
            {
                attackingVillagerColliders.Add(other);
            }
            else
            {
                attackingVillagerColliders.Add(other);
                // only want one 
                attackedVillager = other;
                attackingObject = true;
                // set the character that first entered the box to the new player
                enemyMovementScript.SetPlayer(other.transform);
                // only want this Coroutine to be called once per object enter
                StartCoroutine(nameof(RemoveVillagerHealth));
            }
            // both types of villagers should have their attacking set to true
            other.GetComponent<Villager>().SetAttacking(true);
            
        }

        if (other.CompareTag("Archer"))
        {
            if (attackingObject)
            {
                attackingVillagerColliders.Add(other);   
            }
            else
            {
                attackingArcherColliders.Add(other);
                attackedArcher = other;
                attackingObject = true;
                // set the character that first entered the box to the new player
                enemyMovementScript.SetPlayer(other.transform);
                StartCoroutine(nameof(RemoveArcherHealth));
            }
            other.GetComponent<Archer>().SetAttacking(true);
        }

        if (other.CompareTag("Warrior"))
        {
            if (attackingObject)
            {
                attackingWarriorColliders.Add(other);   
            }
            else
            {
                attackingWarriorColliders.Add(other);
                attackedWarrior = other;
                attackingObject = true;
                // set the character that first entered the box to the new player
                enemyMovementScript.SetPlayer(other.transform);
                StartCoroutine(nameof(RemoveWarriorHealth));
            } 
            other.GetComponent<Warrior>().SetAttacking(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Villager"))
        {
            other.GetComponent<Villager>().SetAttacking(false);
            // remove the reference of the villager that left or was killed
            attackingVillagerColliders.Remove(other);
            // change the state of the attacking to false in the villager that left
            other.GetComponent<Villager>().SetAttacking(false);
            if (attackedVillager == other)
            {
                // remove the reference to the object that left
                attackedVillager = null;
                //  if the villager that was being attacked by the enemy is equal to the villager that left the area
                // where they can be damaged by the enemy, then the enemy needs to find a new target for its attacks
                // I believe that the enemy should attack archers, then warriors, then villagers. So in the method
                // that decides the next attacked object is
                Collider newAttackedObject = GetNewAttackedObject();
                if (newAttackedObject == null)
                {
                    Debug.Log("There are currently no characters to attack in the enemies boxCollider.");   
                }
                else if (newAttackedObject.gameObject.CompareTag("Warrior"))
                {
                    attackedWarrior = newAttackedObject;
                    // set the enemies player to the returned object
                    enemyMovementScript.SetPlayer(newAttackedObject.transform);
                    StartCoroutine(nameof(RemoveWarriorHealth));
                }
                else if (newAttackedObject.gameObject.CompareTag("Archer"))
                {
                    attackedArcher = newAttackedObject;
                    // set the enemies player to the returned object
                    enemyMovementScript.SetPlayer(newAttackedObject.transform);
                    StartCoroutine(nameof(RemoveArcherHealth));
                }
                else if (newAttackedObject.gameObject.CompareTag("Villager"))
                {
                    attackedVillager = newAttackedObject;
                    // set the enemies player to the returned object
                    enemyMovementScript.SetPlayer(newAttackedObject.transform);
                    StartCoroutine(nameof(RemoveVillagerHealth));
                }
            }
        }

        if (other.CompareTag("Archer"))
        {
            other.GetComponent<Archer>().SetAttacking(false);
            // remove the reference of the archer that left or was killed
            attackingArcherColliders.Remove(other);
            // change the state of the attacking to false in the villager that left
            other.GetComponent<Archer>().SetAttacking(false);
            if (attackedArcher == other)
            {
                // remove the reference to the object that left
                attackedArcher = null;
                Collider newAttackedObject = GetNewAttackedObject();
                if (newAttackedObject == null)
                {
                    Debug.Log("There are currently no characters to attack in the enemies boxCollider.");   
                }
                else if (newAttackedObject.CompareTag("Warrior"))
                {
                    attackedWarrior = newAttackedObject;
                    StartCoroutine(nameof(RemoveWarriorHealth));
                }
                else if (newAttackedObject.CompareTag("Archer"))
                {
                    attackedArcher = newAttackedObject;
                    StartCoroutine(nameof(RemoveArcherHealth));
                }
                else if (newAttackedObject.CompareTag("Villager"))
                {
                    attackedVillager = newAttackedObject;
                    StartCoroutine(nameof(RemoveVillagerHealth));
                }
            }
        }

        if (other.CompareTag("Warrior"))
        {
            other.GetComponent<Warrior>().SetAttacking(false);
            // remove the reference of the warrior that left or was killed
            attackingWarriorColliders.Remove(other);
            // change the state of the attacking to false in the villager that left
            other.GetComponent<Warrior>().SetAttacking(false);
            if (attackedWarrior == other)
            {
                // remove the reference to the object that left
                attackedWarrior = null;
                Collider newAttackedObject = GetNewAttackedObject();
                if (newAttackedObject == null)
                {
                    Debug.Log("There are currently no characters to attack in the enemies boxCollider.");
                }
                else if (newAttackedObject.CompareTag("Warrior"))
                {
                    attackedWarrior = newAttackedObject;
                    StartCoroutine(nameof(RemoveWarriorHealth));
                }
                else if (newAttackedObject.CompareTag("Archer"))
                {
                    attackedArcher = newAttackedObject;
                    StartCoroutine(nameof(RemoveArcherHealth));
                }
                else if (newAttackedObject.CompareTag("Villager"))
                {
                    attackedVillager = newAttackedObject;
                    StartCoroutine(nameof(RemoveVillagerHealth));
                }
            }
        }
    }


    private Collider GetNewAttackedObject()
    {
        if (attackingWarriorColliders.Count != 0)
        {
            return attackingWarriorColliders[0];
        }

        if (attackingArcherColliders.Count != 0)
        {
            return attackingArcherColliders[0];
        }

        if (attackingVillagerColliders.Count != 0)
        {
            return attackingVillagerColliders[0];
        }

        // if there are no objects left attacking then return null
        return null;
    }

    private IEnumerator RemoveVillagerHealth()
    {
        WaitForSeconds wait = new WaitForSeconds(enemyScript.GetAttackSpeed());
        // remove health from the enemy
        for (int i = 0; i < attackingVillagerColliders.Count; i++)
        {
            // check whether or not the villager was killed after the last attack so that a null reference exception is not thrown
            if (attackingVillagerColliders[i] != null)
                enemyScript.RemoveHealth(attackingVillagerColliders[i].GetComponent<Villager>().GetDamage());
        }

        if (enemyScript.GetHealth() > 0 && attackedVillager != null)
        {
            // remove health from the attacking villagers
            Villager currVillager = attackedVillager.GetComponent<Villager>();
            currVillager.RemoveHealth(enemyScript.GetDamage());
            if (currVillager.GetHealth() <= 0)
            {
                Destroy(currVillager.gameObject.transform.GetChild(0).GetComponent<BoxCollider>());
                StopCoroutine(nameof(RemoveVillagerHealth));
            }
        }

        yield return wait;
        StartCoroutine(nameof(RemoveVillagerHealth));
    }
    
    private IEnumerator RemoveArcherHealth()
    {
        WaitForSeconds wait = new WaitForSeconds(enemyScript.GetAttackSpeed());
        // remove health from the enemy
        for (int i = 0; i < attackingArcherColliders.Count; i++)
        {
            enemyScript.RemoveHealth(attackingArcherColliders[i].GetComponent<Archer>().GetDamage());
        }

        if (enemyScript.GetHealth() > 0 && attackedArcher != null)
        {
            // remove health from the attacking archer
            Archer currArcher = attackedArcher.GetComponent<Archer>();
            currArcher.RemoveHealth(enemyScript.GetDamage());
            if (currArcher.GetHealth() <= 0)
            {
                // this may need to be changed depending on the archer prefab structure
                Destroy(currArcher.gameObject.transform.GetChild(0).GetComponent<BoxCollider>());
                StopCoroutine(nameof(RemoveArcherHealth));
            }
        }

        yield return wait;
        StartCoroutine(nameof(RemoveArcherHealth));
    }
    
    private IEnumerator RemoveWarriorHealth()
    {
        WaitForSeconds wait = new WaitForSeconds(enemyScript.GetAttackSpeed());
        // remove health from the enemy
        for (int i = 0; i < attackingWarriorColliders.Count; i++)
        {
            enemyScript.RemoveHealth(attackingWarriorColliders[i].GetComponent<Warrior>().GetDamage());
        }

        if (enemyScript.GetHealth() > 0 && attackedWarrior != null)
        {
            // remove health from the attacking warrior
            Warrior currWarrior = attackedWarrior.GetComponent<Warrior>();
            currWarrior.RemoveHealth(enemyScript.GetDamage());
            if (currWarrior.GetHealth() <= 0)
            {
                // this may need to be changed depending on the archer prefab structure
                Destroy(currWarrior.gameObject.transform.GetChild(0).GetComponent<BoxCollider>());
                StopCoroutine(nameof(RemoveWarriorHealth));
            }
        }

        yield return wait;
        StartCoroutine(nameof(RemoveWarriorHealth));
    }
}
