using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TierThreeEnemy : MonoBehaviour
{
    private Enemy currEnemyScript;

    private void Awake()
    {
        currEnemyScript = GetComponent<Enemy>();
        
        currEnemyScript.SetDamage(15);
        currEnemyScript.SetHealth(100);
        currEnemyScript.SetSpeed(2.75f);
    }
}
