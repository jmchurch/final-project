using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.Rendering.HybridV2;
using UnityEditor;
using UnityEngine.AI;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawningDependingOnFogOfWar : MonoBehaviour
{
    public RenderTexture renderTexture;
    public RenderTexture compareTexture;
    public Camera enemySpawnCheckingCamera;
    
    
    public int NumberOfEnemiesSpawn = 100;
    public List<Enemy> enemyPrefabs = new List<Enemy>();
    public SpawnMethod enemySpawnMethod = SpawnMethod.PROCEDURAL;

    private Dictionary<int, ObjectPool> enemyObjectPools = new Dictionary<int, ObjectPool>();
    private PlayerController player;
    private Color[] comparePixels;
    private static Transform townHallTransform;
    private static List<float> spawnCutoffs = new List<float>();

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        comparePixels = new Texture2D(10, 10).GetPixels(0);
        StartCoroutine(nameof(WaitForComparePixelsAssignment));
    }

    private void Awake()
    {
        for (int i = 0; i < enemyPrefabs.Count; i++)
        {
            enemyObjectPools.Add(i, ObjectPool.CreateInstance(enemyPrefabs[i], NumberOfEnemiesSpawn));
        }
    }

    public IEnumerator MoveCameraAround()
    {
        WaitForSeconds wait = new WaitForSeconds(1);
        while (enabled)
        {
            Debug.Log("Moved Camera");
            // get a random x and y point inside a circle with a radius of 20
            Vector2 randPosInCircle = Random.insideUnitCircle * 70;
            // get playerOne
            Player playerOne = player.GetPlayerOne();
            List<Transform> characterTransforms = playerOne.GetCharacterTransforms();


            int rand = Random.Range(0, characterTransforms.Count);
            Vector3 cameraPos = characterTransforms[rand].position +
                                new Vector3(randPosInCircle.x, 70, randPosInCircle.y);
            enemySpawnCheckingCamera.transform.position = cameraPos;
            yield return new WaitForSeconds(.1f);
            // check the renderTexture that is created from this new position, if it is completely black, then spawn an enemy there depending on the distance away from the town hall
            // check whether the enemy can be placed
            Color[] renderTexturePixels = TextureComparison.RenderTextureToTexture2D(renderTexture).GetPixels(0);
            bool areMatching = true;
            for (int j = 0; j < renderTexturePixels.Length; j++)
            {
                if (renderTexturePixels[j] != comparePixels[j])
                {
                    areMatching = false;
                    break;
                }
            }

            RaycastHit hit;
            // makes sure there is a hit
            if (Physics.Raycast(enemySpawnCheckingCamera.transform.position,
                enemySpawnCheckingCamera.transform.forward,
                out hit, 1000))
            {
                // three distances
                // 1: only level one enemies available
                // 2: level one and level two enemies available, level one more likely
                // 3: level two and level three enemies available, level two more likely
                if (areMatching && spawnCutoffs.Count != 0 && !hit.transform.CompareTag("Terrain"))
                {
                    int spawnIndex = 0;
                    if (enemySpawnMethod == SpawnMethod.PROCEDURAL)
                    {
                        // find out what the chances for certain enemy spawns are
                        float distanceFromTownHall = Vector3.Distance(townHallTransform.position, hit.point);
                        if (distanceFromTownHall <= spawnCutoffs[0])
                        {
                            // only level one enemies can spawn
                            spawnIndex = 0;
                        }
                        else if (distanceFromTownHall <= spawnCutoffs[1])
                        {
                            // only  level one and level two enemies can spawn, more likely for level one enemies
                            int enemyChoice = Random.Range(0, 10);
                            if (enemyChoice <= 6)
                            {
                                spawnIndex = 0;
                            }
                            else
                            {
                                spawnIndex = 1;
                            }
                        }
                        else
                        {
                            // only level two and level three enemies can spawn, more likely for level two enemies
                            int enemyChoice = Random.Range(0, 10);
                            if (enemyChoice <= 6)
                            {
                                spawnIndex = 1;
                            }
                            else
                            {
                                spawnIndex = 2;
                            }
                        }
                    }

                    // the two textures are matching, the spawnCutoffs have been created, and the raycast didn't hit a terrain object
                    PoolableObject poolableObject = enemyObjectPools[spawnIndex].GetObject();

                    if (poolableObject != null)
                    {
                        Enemy enemy = poolableObject.GetComponent<Enemy>();

                        NavMeshHit navMeshHit;
                        if (NavMesh.SamplePosition(hit.point, out navMeshHit, 1000, -1))
                        {
                            enemy.agent.Warp(navMeshHit.position);
                            // enemy now need to get enabled and start chasing
                            enemy.agent.enabled = true;
                            enemy.movement.StartChasing();
                        }
                        else
                        {
                            Debug.Log("The hit didn't hit the NavMesh");
                        }
                    }
                    else
                    {
                        Debug.LogError(String.Format(
                            "Unable to spawn object at {0} from object pool. Possibly out of objects", spawnIndex));
                    }
                }
            }
            yield return wait;
        }
    }

    public static void SetSpawnCutoffs(GameObject ground)
    {
        // what extents is: it is half the diagonal distance of a mesh
        // extents.x = half the width
        // extents.y = half the height
        Vector3 bounds = ground.GetComponent<Renderer>().bounds.extents;
        float distance;
        // set the distance to the larger of the two values, might want to change if different spawn distribution is wanted
        if (bounds.x >= bounds.z)
        {
            distance = bounds.x * 2;
        }
        else
        {
            distance = bounds.z * 2;
        }
        spawnCutoffs.Add(distance * (1/3f));
        spawnCutoffs.Add(distance * 2/3f);
    }

    public static void SetTownHallTransform(Transform townhall)
    {
        townHallTransform = townhall;
    }

    private IEnumerator WaitForComparePixelsAssignment()
    {
        yield return new WaitForSeconds(.3f);
        
        Color[] renderTexturePixels = TextureComparison.RenderTextureToTexture2D(renderTexture).GetPixels(0);
        comparePixels = renderTexturePixels;
        Color singlePixel = renderTexturePixels[0];
        comparePixels = TextureComparison.ManipulatePixels(comparePixels, singlePixel.r, singlePixel.g, singlePixel.b, singlePixel.a);
        String colorPixels = "";
        for (int i = 0; i < comparePixels.Length; i++)
        {
            colorPixels += " " + comparePixels[i];
        }
        Debug.Log("ComparePixels " + colorPixels);
    }

    public enum SpawnMethod
    {
        PROCEDURAL
    }
}
