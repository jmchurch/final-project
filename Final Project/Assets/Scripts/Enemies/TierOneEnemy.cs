using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TierOneEnemy : MonoBehaviour
{
    private Enemy currEnemyScript;

    private void Awake()
    {
        currEnemyScript = GetComponent<Enemy>();
        
        currEnemyScript.SetDamage(10);
        currEnemyScript.SetHealth(50);
        currEnemyScript.SetSpeed(2.5f);
    }
}
