using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using DG.Tweening;

public class TerrainSpawning : MonoBehaviour
{
    /*
        Index 0: floating square ground, no walls
        Index 1: fake triangle, facing left, two walls
        Index 2: fake triangle, facing right, two walls
        Index 3: Triangle, two walls
        Index 4: floating square ground, one wall
        Index 5: floating square ground, two walls
        Index 6: floating square ground, three walls
        
        Size should defualt to  15
        
        structure of prefabs
        Index 0: model
        Index 1: parent of all active positions
            Index 0 - 4: empty GameObjects (could be as little as 1 index) 
        Index 2: parent of all inactive positions
            Index 0 - 3: empty GameObjects (could have no indexes)
        
        At the moment, I want the map to be 2km^2
        
    */
    
    public TerrainBlock[] terrainBlocks;
    public Element[] collectibleElements;
    public GameObject townHallPrefab;
    public GameObject cam;
    public Camera tempCamera;
    public PlayerController player;
    public GameObject collectibles;
    public Material groundMaterial;

    private List<Vector3> inactivePositions;
    private Queue<Vector3> terrainSpawnPoints;
    private List<Vector3> takenPoints;
    private List<GameObject> spawnedTerrainBlocks;

    [SerializeField] private int terrainCap = 1000;
    [SerializeField] private int terrainIslands = 10;

    private GameObject currParent;
    private Grid grid;

    // TerrainBlock objects
    [System.Serializable]
    public class TerrainBlock
    {
        // object name
        public string name;
        // object model
        public GameObject prefab;
        public int size;
    }
    
    // Tree and Rock objects
    [System.Serializable]
    public class Element
    {
        public string name;
        public GameObject prefab;
    }

    // Start is called before the first frame update
    void Start()
    {
        grid = GameObject.Find("Grid").GetComponent<Grid>();
        inactivePositions = new List<Vector3>();
        terrainSpawnPoints = new Queue<Vector3>();
        takenPoints = new List<Vector3>();
        spawnedTerrainBlocks = new List<GameObject>();
        // start by creating the root ground piece
        TerrainBlock root = terrainBlocks[Random.Range(0, terrainBlocks.Length - 1)];
        // block that will act as the root to all other blocks has been spawned
        GameObject rootBlock = Instantiate(root.prefab);
        // set the start terrain block that will act as the parents for the other blocks
        // in its island
        currParent = rootBlock;
        rootBlock.transform.SetParent(transform);
        rootBlock.transform.position = transform.position;
        // put the root block on the Terrain layer so that it is ignored by the projector
        rootBlock.layer = 7;
        takenPoints.Add(rootBlock.transform.position);
        spawnedTerrainBlocks.Add(rootBlock);
        rootBlock.isStatic = true;

        // find all the inactive points
        Transform inactivePointParent = rootBlock.transform.GetChild(2);
        for (int i = 0; i < inactivePointParent.childCount; i++)
        {
            // add all of the points that TerrainBlocks can't be spawned on
            inactivePositions.Add(inactivePointParent.GetChild(i).position);
        }
        
        // find all the open points
        Transform activePointParent = rootBlock.transform.GetChild(1);
        for (int i = 0; i < activePointParent.childCount; i++)
        {
            Vector3 currPos = activePointParent.GetChild(i).position;
            if (!takenPoints.Contains(currPos))
            {
                takenPoints.Add(currPos);
                terrainSpawnPoints.Enqueue(currPos);
            }
        }
        
        SpawnTerrainBlock();

        // at each point:
        // create a new TerrainBlock, Instantiate the prefab, set the position to the open point, rotate the block to be facing the correct direction
        
        // then find all the open points
        // at each point:
        // create a new TerrainBlock, Instantiate the prefab, set the position to the open point, rotate the block to be facing the correct direction
    }

    private void SpawnTerrainBlock()
    {
        int terrainMasses = 0;
        int count = 0;
        while (terrainSpawnPoints.Count != 0 && count < terrainCap && terrainMasses < terrainIslands)
        {
            count++;
            // get the first spawnPoint in the Queue
            Vector3 spawnPos = terrainSpawnPoints.Dequeue();
            int index = Random.Range(0, terrainBlocks.Length - 1);
            GameObject terrainObject = new GameObject();
            terrainObject.name = String.Format("{0}", count);

            int countTwo = 0;
            bool placed = false;
            while (true)
            {
                countTwo++;
                // will loop through all the possible blocks, use modulus to make sure it doesn't go over
                index = (index + 1) % (terrainBlocks.Length);

                // create the terrainObject off of the index of TerrainBlocks
                Destroy(terrainObject);
                terrainObject = Instantiate(terrainBlocks[index].prefab);
                terrainObject.transform.position = spawnPos;
                terrainObject.name = String.Format("{0}", count);


                // set the position of the terrainObject
                terrainObject.transform.position = spawnPos;
                // set the parent to currParent transform so that it can be enabled and disabled
                // with more ease (less looping over terrain children)
                terrainObject.transform.SetParent(currParent.transform);

                // rotate the gameObject so that one of the open walls is facing the open wall of its parent
                // i.e. one of the activePoint GameObject's positions will be equal to parentPos

                List<Transform> currObjectActivePoints = new List<Transform>();
                Transform activePointsParent = terrainObject.transform.GetChild(1);
                int childCount = activePointsParent.childCount;
                // get references to all the activePoint GameObjects
                for (int i = 0; i < childCount; i++)
                {
                    currObjectActivePoints.Add(activePointsParent.GetChild(i));
                }

                // making a list of inactive point transforms
                List<Transform> currObjectInactivePoints = new List<Transform>();
                // find all the inactive points
                Transform inactivePointParent = terrainObject.transform.GetChild(2);
                for (int i = 0; i < inactivePointParent.childCount; i++)
                {
                    // add all of the Transforms to the inactivePoints list
                    currObjectInactivePoints.Add(inactivePointParent.GetChild(i));
                }

                // Method determines whether any rotation of the gameObject is viable for placement
                if (OpenLocation(currObjectActivePoints, currObjectInactivePoints, terrainObject, true))
                {
                    // add all the active points and inactive points from the current terrain block to their respective lists
                    QueueActivePoints(currObjectActivePoints, currObjectInactivePoints);
                    // ass the current terrain block to the list holding all placed terrain blocks
                    spawnedTerrainBlocks.Add(terrainObject);
                    // make the terrainObject static and put it on the terrain layer so it is ignored by the projectors
                    // the actual model has to be on the terrain layer ****
                    terrainObject.layer = 7;
                    terrainObject.isStatic = true;

                    // take the last inactive point that was added, add 60 to it in each direction.
                    // if that point is not covered by an active point or an inactive point then spawn the new root there
                    // otherwise, check the next index of inactive points and so on...
                    if (terrainSpawnPoints.Count == 0 && terrainMasses != terrainIslands - 1)
                    {
                        // the current island is finished and a new island is starting
                        // in the new island method we will need to update the currParent so
                        // that the new terrain blocks will be classified under the correct parent
                        
                        float largestX = 0;
                        int largeXIndex = 0;
                        float largestZ = 0;
                        int largeZIndex = 0;
                        float smallestX = 0;
                        int smallXIndex = 0;
                        float smallestZ = 0;
                        int smallZIndex = 0;

                        // find the maximum inactive point in each direction
                        for (int i = 0; i < inactivePositions.Count; i++)
                        {
                            Vector3 currVector3 = inactivePositions[i];
                            if (currVector3.x > largestX && currVector3.z < 1000 && currVector3.z > -1000 && currVector3.x < 1000)
                            {
                                largestX = inactivePositions[i].x;
                                largeXIndex = i;
                            }
                            if (currVector3.x < smallestX && currVector3.z < 1000 && currVector3.z > -1000 && currVector3.x > -1000)
                            {
                                smallestX = inactivePositions[i].x;
                                smallXIndex = i;
                            }

                            if (currVector3.z > largestZ && currVector3.x > -1000 && currVector3.x < 1000 && currVector3.z < 1000)
                            {
                                largestZ = inactivePositions[i].z;
                                largeZIndex = i;
                            }

                            if (currVector3.z < smallestZ && currVector3.x > -1000 && currVector3.x < 1000 && currVector3.z > -1000)
                            {
                                smallestZ = inactivePositions[i].z;
                                smallZIndex = i;
                            }
                        }
                        
                        // randomly pick between all the directions
                        int direction = Random.Range(0, 4);
                        // right
                        if (direction == 0)
                        {
                            terrainMasses += StartNewTerrainIsland(largeXIndex);
                        }
                        // left
                        if (direction == 1)
                        {
                            terrainMasses += StartNewTerrainIsland(smallXIndex);
                        }
                        // up
                        if (direction == 2)
                        {
                            terrainMasses += StartNewTerrainIsland(largeZIndex);
                        }
                        // down
                        if (direction == 3)
                        {
                            terrainMasses += StartNewTerrainIsland(smallZIndex);
                        }
                    }
                    
                    // end the while true loop
                    break;
                }
            }
        }

        foreach (Vector3 inactivePoint in inactivePositions)
        {
            // loop through all the inactive points, with a 50% chance to spawn trees around that point
            int num = Random.Range(0, 2);
            if (num == 0)
            {
                // call a method to spawn collectibles randomly around that point
                SpawnCollectibles(inactivePoint);
            }
        }
        
        
        // Initialize variable to find the corners of the terrain
        float top = 0;
        float left = 0;
        float bottom = 0;
        float right = 0;
        // create a ground gameObject (plane)
        GameObject ground = GameObject.CreatePrimitive(PrimitiveType.Plane);
        foreach (Vector3 inactivePoint in inactivePositions)
        {
            if (inactivePoint.z > top)
            {
                top = inactivePoint.z;
            }

            if (inactivePoint.z < bottom)
            {
                bottom = inactivePoint.z;
            }

            if (inactivePoint.x < left)
            {
                left = inactivePoint.x;
            }

            if (inactivePoint.x > right)
            {
                right = inactivePoint.x;
            }
        }
        
        Vector3 topRight = new Vector3(right, 0, top);
        Vector3 topLeft = new Vector3(left, 0, top);
        Vector3 bottomRight = new Vector3(right, 0, bottom);
        Vector3 bottomLeft = new Vector3(left, 0, bottom);
        // put the plane at the half way point between the two positions
        ground.transform.position = Vector3.Lerp(topRight, bottomLeft, .5f);
        float width = Vector3.Distance(bottomLeft, bottomRight);
        float height = Vector3.Distance(bottomLeft, topLeft);
        ground.transform.localScale = new Vector3(width / 9, 1f, height / 9);
        Destroy(ground.GetComponent<MeshCollider>());
        ground.AddComponent<BoxCollider>();
        ground.tag = "Ground";
        ground.name = "Ground";
        ground.isStatic = true;
        ground.AddComponent<NavMeshSourceTag>();
        ground.GetComponent<Renderer>().material = groundMaterial;
        // set the spawnCutoff points 
        EnemySpawningDependingOnFogOfWar.SetSpawnCutoffs(ground);
        // set the two projectors to the center of the map
        Vector3 groundPosition = ground.transform.position;
        GameObject.Find("DarkFogProjector").transform.position = groundPosition;
        GameObject.Find("FogProjector").transform.position = groundPosition;
        // set the position of the cameras taking in the fog of war data
        GameObject.Find("ExploredAreasCam").transform.position = groundPosition + new Vector3(0, 640, 0);
        GameObject.Find("UnexploredAreasCam").transform.position = groundPosition + new Vector3(0, 640, 0);
        // create camera that will be used to place the town hall
        tempCamera.transform.position = groundPosition + new Vector3(0, (width + height) / 2, 0);
        tempCamera.transform.eulerAngles = new Vector3(90, 0, 0);
        // create the gameObject that allows for real time navMesh changes
        GameObject navMeshBuilder = new GameObject();
        navMeshBuilder.name = "Nav Mesh Builder";
        navMeshBuilder.transform.position = groundPosition;
        navMeshBuilder.AddComponent<LocalNavMeshBuilder>().m_Size = new Vector3(width * 1.25f, 100, height * 1.25f);

        // add the terrain positions to playerOne's state
        Player playerOne = player.GetPlayerOne();
        foreach (GameObject terrainBlock in spawnedTerrainBlocks)
        {
            playerOne.AddTerrainPoint(terrainBlock.transform.position + new Vector3(0,4,0));
        }

        GameObject townHall = GameObject.Find("ObjectPlacer").GetComponent<ObjectPlacer>().InstantiateTownHallObject();
        if (townHall != null)
        {
            // set the parent of the RTS camera to the townHall
            cam.transform.SetParent(townHall.transform);
            cam.transform.position = townHall.transform.GetChild(0).position;
        }

    }

    private bool OpenLocation(List<Transform> currObjectActivePoints, List<Transform> currObjectInactivePoints, GameObject terrainObject, bool connectionMatters)
    {
        for (int i = 0; i <= 270; i += 90)
        {
            // rotate the child GameObject to the i value
            terrainObject.transform.eulerAngles = new Vector3(0, i, 0);

            bool pointUsed = false;
            bool terrainConnected = false;
            bool terrainBlocked = false;
            // checking whether the active points are covering inactive points

            foreach (Vector3 inactivePoint in inactivePositions)
            {
                // want pointUsed to be FALSE
                foreach (Transform activePoint in currObjectActivePoints)
                {
                    float x = inactivePoint.x;
                    float z = inactivePoint.z;
                    Vector3 currPos = activePoint.position;
                    float pX = currPos.x;
                    float pZ = currPos.z;
                    float xDiff = Math.Abs(x - pX);
                    float zDiff = Math.Abs(z - pZ);
                    if (xDiff < 2 && zDiff < 2)
                    {
                        pointUsed = true;
                    }
                }
            }

            // CODE IF THERE WAS NO TINY OFFSET IN TERRAIN BLOCK PLACEMENT

            // foreach (Transform activePoint in currObjectActivePoints)
            // {
            //     if (takenPoints.Contains(activePoint.position))
            //     {
            //         terrainConnected = true;
            //     }
            //
            //     if (inactivePositions.Contains(activePoint.position))
            //     {
            //         pointUsed = true;
            //     }
            // }
            //
            // foreach (Transform inactivePoint in currObjectInactivePoints)
            // {
            //     if (takenPoints.Contains(inactivePoint.position))
            //     {
            //         terrainBlocked = true;
            //     }
            // }

            // looping through all the points that are covered by terrainBlocks already
            foreach (Vector3 takenPoint in takenPoints)
            {
                // comparing the covered points to the activePoints of the current terrainBlock
                // want terrainConnected to be TRUE
                foreach (Transform activePoint in currObjectActivePoints)
                {
                    float x = takenPoint.x;
                    float z = takenPoint.z;
                    Vector3 currPos = activePoint.position;
                    float pX = currPos.x;
                    float pZ = currPos.z;
                    float xDiff = Math.Abs(x - pX);
                    float zDiff = Math.Abs(z - pZ);
                    if (xDiff < 2 && zDiff < 2)
                    {
                        terrainConnected = true;
                    }
                }

                // this will be true when the connection doesn't matter
                // i.e. a new terrain island is being created
                if (!connectionMatters)
                {
                    terrainConnected = true;
                }


                // comparing the covered points to the inactivePoints of the current terrainBlock
                // want terrainBlocked to be FALSE
                foreach (Transform inactivePoint in currObjectInactivePoints)
                {
                    float x = takenPoint.x;
                    float z = takenPoint.z;
                    Vector3 currPos = inactivePoint.position;
                    float pX = currPos.x;
                    float pZ = currPos.z;
                    float xDiff = Math.Abs(x - pX);
                    float zDiff = Math.Abs(z - pZ);
                    if (xDiff < 2 && zDiff < 2)
                    {
                        terrainBlocked = true;
                    }
                }
            }

            // POSSIBLE PROBLEM: right now I think that it will just place down a block even if there are multiple taken points around it
            // that it needs to fill up

            // if the active points are not over an inactive point
            // at least one active point is hitting a taken point
            // no inactive points are hitting active points
            if (!pointUsed && terrainConnected && !terrainBlocked)
            {
                return true;
            }
            // rotate again, or if all the rotations are done, try a new terrainBlock
        }
        return false;
    }

    private void QueueActivePoints(List<Transform> currObjectActivePoints, List<Transform> currObjectInactivePoints)
    {
        foreach (Transform point in currObjectActivePoints)
        {
            // add the points to the terrainSpawnPoints Queue
            Vector3 currPos = point.position;
            bool pointUsed = false;
            foreach (Vector3 takenPosition in takenPoints)
            {
                float x = takenPosition.x;
                float z = takenPosition.z;
                float pX = currPos.x;
                float pZ = currPos.z;
                float xDiff = Math.Abs(x - pX);
                float zDiff = Math.Abs(z - pZ);
                if (xDiff < 5 && zDiff < 5)
                {
                    pointUsed = true;
                }
            }

            if (!pointUsed)
            {
                takenPoints.Add(currPos);
                terrainSpawnPoints.Enqueue(currPos);
            }
        }

        foreach (Transform inactivePoint in currObjectInactivePoints)
        {
            bool pointUsed = false;
            foreach (Vector3 point in inactivePositions)
            {
                float x = inactivePoint.position.x;
                float z = inactivePoint.position.z;
                float pX = point.x;
                float pZ = point.z;
                float xDiff = Math.Abs(x - pX);
                float zDiff = Math.Abs(z - pZ);
                if (xDiff < 5 && zDiff < 5)
                {
                    pointUsed = true;
                }
            }

            if (!pointUsed)
            {
                inactivePositions.Add(inactivePoint.position);   
            }
        }
    }

    private int StartNewTerrainIsland(int index)
    {
        for (int i = 0; i < 4; i++)
        {
            Vector3 currPos = inactivePositions[index];
            if (index + i < inactivePositions.Count - 1)
                currPos = inactivePositions[index + i];

            GameObject rootBlock = currParent = Instantiate(terrainBlocks[Random.Range(0, terrainBlocks.Length - 1)].prefab);
            
            rootBlock.transform.SetParent(transform);
            spawnedTerrainBlocks.Add(rootBlock);

            List<Transform> currObjectActivePoints = new List<Transform>();
            Transform activePointsParent = rootBlock.transform.GetChild(1);
            int childCount = activePointsParent.childCount;
            // get references to all the activePoint GameObjects
            for (int j = 0; j < childCount; j++)
            {
                currObjectActivePoints.Add(activePointsParent.GetChild(j));
            }

            // making a list of inactive point transforms
            List<Transform> currObjectInactivePoints = new List<Transform>();
            // find all the inactive points
            Transform inactivePointParent = rootBlock.transform.GetChild(2);
            for (int j = 0; j < inactivePointParent.childCount; j++)
            {
                // add all of the Transforms to the inactivePoints list
                currObjectInactivePoints.Add(inactivePointParent.GetChild(j));
            }

            // booleans to keep track of whether or no the position has been taken
            bool right = false;
            bool left = false;
            bool up = false;
            bool down = false;

            foreach (Vector3 inactivePoint in inactivePositions)
            {
                float rightX = (currPos + new Vector3(240, 0, 0)).x;
                float rightZ = (currPos + new Vector3(240, 0, 0)).z;

                float leftX = (currPos + new Vector3(-240, 0, 0)).x;
                float leftZ = (currPos + new Vector3(-240, 0, 0)).z;

                float upX = (currPos + new Vector3(0, 0, 240)).x;
                float upZ = (currPos + new Vector3(0, 0, 240)).z;

                float downX = (currPos + new Vector3(0, 0, -240)).x;
                float downZ = (currPos + new Vector3(0, 0, -240)).z;

                float x = inactivePoint.x;
                float z = inactivePoint.z;

                float rightXDiff = Math.Abs(x - rightX);
                float rightZDiff = Math.Abs(z - rightZ);

                float leftXDiff = Math.Abs(x - leftX);
                float leftZDiff = Math.Abs(z - leftZ);

                float upXDiff = Math.Abs(x - upX);
                float upZDiff = Math.Abs(z - upZ);

                float downXDiff = Math.Abs(x - downX);
                float downZDiff = Math.Abs(z - downZ);


                if (rightZDiff < 2 && rightXDiff < 2)
                {
                    right = true;
                }

                if (leftXDiff < 2 && leftZDiff < 2)
                {
                    left = true;
                }

                if (upXDiff < 2 && upZDiff < 2)
                {
                    up = true;
                }

                if (downXDiff < 2 && downZDiff < 2)
                {
                    down = true;
                }

            }

            foreach (Vector3 takenPoint in takenPoints)
            {
                float rightX = (currPos + new Vector3(240, 0, 0)).x;
                float rightZ = (currPos + new Vector3(240, 0, 0)).z;

                float leftX = (currPos + new Vector3(-240, 0, 0)).x;
                float leftZ = (currPos + new Vector3(-240, 0, 0)).z;

                float upX = (currPos + new Vector3(0, 0, 240)).x;
                float upZ = (currPos + new Vector3(0, 0, 240)).z;

                float downX = (currPos + new Vector3(0, 0, -240)).x;
                float downZ = (currPos + new Vector3(0, 0, -240)).z;

                float x = takenPoint.x;
                float z = takenPoint.z;

                float rightXDiff = Math.Abs(x - rightX);
                float rightZDiff = Math.Abs(z - rightZ);

                float leftXDiff = Math.Abs(x - leftX);
                float leftZDiff = Math.Abs(z - leftZ);

                float upXDiff = Math.Abs(x - upX);
                float upZDiff = Math.Abs(z - upZ);

                float downXDiff = Math.Abs(x - downX);
                float downZDiff = Math.Abs(z - downZ);


                if (rightZDiff < 2 && rightXDiff < 2)
                {
                    right = true;
                }

                if (leftXDiff < 2 && leftZDiff < 2)
                {
                    left = true;
                }

                if (upXDiff < 2 && upZDiff < 2)
                {
                    up = true;
                }

                if (downXDiff < 2 && downZDiff < 2)
                {
                    down = true;
                }

            }

            if (!right)
            {
                currPos = currPos + new Vector3(240, 0, 0);
                rootBlock.transform.position = currPos;
                if (OpenLocation(currObjectActivePoints, currObjectInactivePoints, rootBlock, false))
                {
                    takenPoints.Add(currPos);
                    //QueueActivePoints(currObjectActivePoints, currObjectInactivePoints);
                    // find all the inactive points
                    Transform inacParent = rootBlock.transform.GetChild(2);
                    for (int j = 0; j < inacParent.childCount; j++)
                    {
                        // add all of the points that TerrainBlocks can't be spawned on
                        inactivePositions.Add(inacParent.GetChild(j).position);
                    }

                    // find all the open points
                    Transform acParent = rootBlock.transform.GetChild(1);
                    for (int j = 0; j < acParent.childCount; j++)
                    {
                        Vector3 acPoint = acParent.GetChild(j).position;
                        if (!takenPoints.Contains(acPoint))
                        {
                            takenPoints.Add(acPoint);
                            terrainSpawnPoints.Enqueue(acPoint);
                        }
                    }

                    rootBlock.layer = 7;
                    rootBlock.isStatic = true;
                    return 1;
                }
            }
            else if (!left)
            {
                currPos = currPos + new Vector3(-240, 0, 0);
                rootBlock.transform.position = currPos;
                takenPoints.Add(currPos);
                if (OpenLocation(currObjectActivePoints, currObjectInactivePoints, rootBlock, false))
                {
                    takenPoints.Add(currPos);
                    //QueueActivePoints(currObjectActivePoints, currObjectInactivePoints);
                    // find all the inactive points
                    Transform inacParent = rootBlock.transform.GetChild(2);
                    for (int j = 0; j < inacParent.childCount; j++)
                    {
                        // add all of the points that TerrainBlocks can't be spawned on
                        inactivePositions.Add(inacParent.GetChild(j).position);
                    }

                    // find all the open points
                    Transform acParent = rootBlock.transform.GetChild(1);
                    for (int j = 0; j < acParent.childCount; j++)
                    {
                        Vector3 acPoint = acParent.GetChild(j).position;
                        if (!takenPoints.Contains(acPoint))
                        {
                            takenPoints.Add(acPoint);
                            terrainSpawnPoints.Enqueue(acPoint);
                        }
                    }
                    rootBlock.layer = 7;
                    rootBlock.isStatic = true;
                    return 1;
                }
            }
            else if (!up)
            {
                currPos = currPos + new Vector3(0, 0, 240);
                rootBlock.transform.position = currPos;
                takenPoints.Add(currPos);
                if (OpenLocation(currObjectActivePoints, currObjectInactivePoints, rootBlock, false))
                {
                    //QueueActivePoints(currObjectActivePoints, currObjectInactivePoints);
                    // find all the inactive points
                    Transform inacParent = rootBlock.transform.GetChild(2);
                    for (int j = 0; j < inacParent.childCount; j++)
                    {
                        // add all of the points that TerrainBlocks can't be spawned on
                        inactivePositions.Add(inacParent.GetChild(j).position);
                    }

                    // find all the open points
                    Transform acParent = rootBlock.transform.GetChild(1);
                    for (int j = 0; j < acParent.childCount; j++)
                    {
                        Vector3 acPoint = acParent.GetChild(j).position;
                        if (!takenPoints.Contains(acPoint))
                        {
                            takenPoints.Add(acPoint);
                            terrainSpawnPoints.Enqueue(acPoint);
                        }
                    }
                    rootBlock.layer = 7;
                    rootBlock.isStatic = true;
                    return 1;
                }
            }
            else if (!down)
            {
                currPos = currPos + new Vector3(0, 0, -240);
                rootBlock.transform.position = currPos;
                takenPoints.Add(currPos);
                if (OpenLocation(currObjectActivePoints, currObjectInactivePoints, rootBlock, false))
                {
                    takenPoints.Add(currPos);
                    //QueueActivePoints(currObjectActivePoints, currObjectInactivePoints);
                    // find all the inactive points
                    Transform inacParent = rootBlock.transform.GetChild(2);
                    for (int j = 0; j < inacParent.childCount; j++)
                    {
                        // add all of the points that TerrainBlocks can't be spawned on
                        inactivePositions.Add(inacParent.GetChild(j).position);
                    }

                    // find all the open points
                    Transform acParent = rootBlock.transform.GetChild(1);
                    for (int j = 0; j < acParent.childCount; j++)
                    {
                        Vector3 acPoint = acParent.GetChild(j).position;
                        if (!takenPoints.Contains(acPoint))
                        {
                            takenPoints.Add(acPoint);
                            terrainSpawnPoints.Enqueue(acPoint);
                        }
                    }
                    rootBlock.layer = 7;
                    rootBlock.isStatic = true;
                    return 1;
                }
            }
            spawnedTerrainBlocks.Remove(rootBlock);
            Destroy(rootBlock);
        }

        return 0;
    }

    private void SpawnCollectibles(Vector3 currPos)
    {
        GameObject collectibleArea = new GameObject();
        // find the terrain block that this collectible area should be a child of
        collectibleArea.transform.SetParent(collectibles.transform);

        collectibleArea.name = "Collectible Area";
        // collectibles will be spawned in a square 12 right, 12 left, 12 up, and 12 down
        Vector3 bottomLeftSpawnPos = currPos + new Vector3(-10, 0, -10);
        for (int i = 0; i <= 10; i += 5)
        {
            for (int j = 0; j <= 10; j += 5)
            {
                // randomly decide whether to spawn a collectible on the position.
                // can change this to also deciding what type of collectible when more collectibles are added in
                int num = Random.Range(0, 4);
                if (num == 0)
                {
                    int rd = Random.Range(0, 5);
                    Element element;
                    // randomly choose the collectible elemnt
                    if (rd < 3)
                        element = collectibleElements[0];
                    else
                        element = collectibleElements[1];
                    
                    Vector3 position = bottomLeftSpawnPos + new Vector3(i, 0f, j);
                    Vector3 offset = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f));
                    Vector3 rotation = new Vector3(Random.Range(-95f, -90f), Random.Range(0f, 360f), Random.Range(0f, 5f));
                    
                    GameObject newElement = Instantiate(element.prefab, position + offset + element.prefab.transform.position + new Vector3(0,-0.8f,0), element.prefab.transform.rotation, collectibleArea.transform);

                    // set the trees rotation to the random rotation
                    if (element == collectibleElements[0] || element == collectibleElements[1])
                    {
                        newElement.transform.eulerAngles = rotation;
                    }

                    newElement.name = "Collectible";
                }
            }
        }
    }

    // returns false when finished (i.e. we are out of the start section)
    public bool TownHallPlaced(Vector3 townHallPosition, Camera overheadCamera)
    {
        // give the transform of the townHall to the EnemySpawningDependingOnFogOfWar script
        EnemySpawningDependingOnFogOfWar.SetTownHallTransform(player.GetPlayerOne().GetTownHall().transform);
        
        // need to: unpause game, tween between overhead camera and the RTS camera, enable the UI Canvas, Spawn the villagers around the TownHall
        // get reference to overHeadCamera currLocation and currRotation
        Transform overheadCamTransform = overheadCamera.transform;
        Vector3 currLocation = overheadCamTransform.position;
        Vector3 currRotation = overheadCamTransform.eulerAngles;

        // Instantiate starting villagers
        player.InstantiateVillager(townHallPosition + new Vector3(13,0,0));
        player.InstantiateVillager(townHallPosition + new Vector3(-13,0,0));
        player.InstantiateVillager(townHallPosition + new Vector3(0,0,13));
        player.InstantiateVillager(townHallPosition + new Vector3(0,0,-13));


        // unpause
        Time.timeScale = 1;
        // enable the UI
        UIScript.EnableMainUICanvas();
        // tween between the currLocation of the
        overheadCamera.transform.DOMove(cam.transform.GetChild(0).position + new Vector3(0,0,-16f), 5);
        overheadCamera.transform.DORotate(cam.transform.GetChild(0).eulerAngles, 5);
        cam.SetActive(true);
        StartCoroutine(SwitchToNewCam(5, overheadCamera, cam.GetComponentInChildren<Camera>(), true));
        overheadCamTransform.position = currLocation;
        overheadCamTransform.eulerAngles = currRotation;
        return false;
    }

    public IEnumerator SwitchToNewCam(float duration, Camera oldCam, Camera newCam, bool switchingToRTS)
    {
        yield return new WaitForSeconds(duration);
        oldCam.enabled = false;
        newCam.enabled = true;
        // set all the cameras in ObjectPlacer
        StartCoroutine(nameof(StartSpawningEnemies));
        if (switchingToRTS)
        {
            GameObject.Find("ObjectPlacer").GetComponent<ObjectPlacer>().SetRTSCam(newCam);
            GameObject.Find("ObjectPlacer").GetComponent<ObjectPlacer>().SetOverheadCam(oldCam);
        }
        else
        {
            GameObject.Find("ObjectPlacer").GetComponent<ObjectPlacer>().SetRTSCam(oldCam);
            GameObject.Find("ObjectPlacer").GetComponent<ObjectPlacer>().SetOverheadCam(newCam);
        }
    }

    private IEnumerator StartSpawningEnemies()
    {
        yield return new WaitForSeconds(2);
        StartCoroutine(GameObject.Find("ProceduralEnemySpawner").GetComponent<EnemySpawningDependingOnFogOfWar>().MoveCameraAround());
    }

    // void OnDrawGizmos()
    // {
    //     // Active Point: Yellow Sphere
    //     // Inactive Point: Red Sphere
    //     // Taken Point: Magenta Sphere
    //     foreach (Vector3 position in inactivePositions)
    //     {
    //         Gizmos.color = Color.yellow;
    //         Gizmos.DrawSphere(position, 2);
    //     }
    //     
    //     foreach (Vector3 position in takenPoints)
    //     {
    //         Gizmos.color = Color.red;
    //         Gizmos.DrawSphere(position, 2);
    //     }
    // }
}
