using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionBox
{
    public GameObject hitboxObj; // public so that it can be referenced and destroyed

    private BoxCollider selectionBox;
    private Vector3 location;
    private float widthX;
    private float widthZ;
    private float heightY;

    public SelectionBox(Vector3 location, float widthX, float widthZ, float heightY)
    {
        // initialize the location, widthX, widthZ, and heightY
        this.location = location;
        this.widthX = widthX;
        this.widthZ = widthZ;
        this.heightY = heightY;
        
        // Set up the hitboxObj and set the location that it will be at
        hitboxObj = new GameObject();
        // set name of the game obj
        hitboxObj.name = "Selection Box";
        // set tag of the game obj
        hitboxObj.tag = "SelectionBox";
        hitboxObj.transform.position = location;
        // add a BoxCollider to the Selection Box GameObject
        selectionBox = hitboxObj.AddComponent(typeof(BoxCollider)) as BoxCollider;
        // set the size off of the widthX, widthZ, and heightY
        if (selectionBox != null)
        {
            // set the size
            selectionBox.size = new Vector3(widthX, heightY, widthZ);
            // set the BoxCollider as a trigger
            selectionBox.isTrigger = true;
        }
        else
        {
            Debug.Log("selectionBox is null");
        }
    }
}
