using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

public class Collision : MonoBehaviour
{

    private ObjectSelection objectSelectionScript;
    private ObjectPlacer objectPlacerScript;
    private PlayerController player;
    private bool townhallRange = false;
    private bool coroutineStarted = false;
    
    
    private void Awake()
    {
        objectSelectionScript = GameObject.FindGameObjectWithTag("SelectionController").GetComponent<ObjectSelection>();
        objectPlacerScript = GameObject.FindGameObjectWithTag("ObjectPlacer").GetComponent<ObjectPlacer>();
        player = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    private void FixedUpdate()
    {
        if (townhallRange && !coroutineStarted)
        {
            if (gameObject.CompareTag("Villager"))
            {
                Villager villager = gameObject.GetComponent<Villager>();
                // the villager has wood in their inventory
                if (villager.GetWoodAmount() != 0)
                {
                    StartCoroutine(AddWood());
                }
                // the villager has stone in their inventory
                else if (villager.GetStoneAmount() != 0)
                {
                    StartCoroutine(AddStone());
                }
            }
            else if (gameObject.CompareTag("Archer"))
            {
                Archer archer = gameObject.GetComponent<Archer>();
                // the villager has wood in their inventory
                if (archer.GetWoodAmount() != 0)
                {
                    StartCoroutine(AddWood());
                }
                // the villager has stone in their inventory
                else if (archer.GetStoneAmount() != 0)
                {
                    StartCoroutine(AddStone());
                }
            }
            else if (gameObject.CompareTag("Warrior"))
            {
                Warrior warrior = gameObject.GetComponent<Warrior>();
                // the villager has wood in their inventory
                if (warrior.GetWoodAmount() != 0)
                {
                    StartCoroutine(AddWood());
                }
                // the villager has stone in their inventory
                else if (warrior.GetStoneAmount() != 0)
                {
                    StartCoroutine(AddStone());
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("SelectionBox"))
        {
            objectSelectionScript.AddGameObject(gameObject);
        }

        if (other.CompareTag("HouseOne"))
        {
            Debug.Log("Collision entered");
            objectPlacerScript.SetCoveringCharacter(true);
        }

        if (other.CompareTag("Barracks"))
        {
            objectPlacerScript.SetCoveringCharacter(true);
        }

        if (other.transform.root.CompareTag("TownHall"))
        {
            if (gameObject.CompareTag("Villager") || gameObject.CompareTag("Warrior") ||
                gameObject.CompareTag("Archer"))
            {
                townhallRange = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("HouseOne"))
        {
            Debug.Log("Collision exited");
            objectPlacerScript.SetCoveringCharacter(false);
        }

        if (other.CompareTag("Barracks"))
        {
            objectPlacerScript.SetCoveringCharacter(false);
        }
        
        if (other.transform.root.CompareTag("TownHall"))
        {
            townhallRange = false;
        }
    }

    public IEnumerator AddWood()
    {
        coroutineStarted = true;
        Player playerOne = player.GetPlayerOne();
        if (gameObject.CompareTag("Villager"))
        {
            Villager villager = gameObject.GetComponent<Villager>();
            // wait three seconds
            yield return new WaitForSeconds(3);
            // remove a wood from the villager's inventory
            villager.RemoveWood(1);
            // add 5 wood to the townhalll
            playerOne.SetWood(playerOne.GetWood() + 5);
            // the villager still has wood and is in the townhall range
            if (villager.GetWoodAmount() != 0 && townhallRange)
            {
                StartCoroutine(AddWood());
            }
            else
            {
                // the villager has stone and is still in the townhall range
                if (villager.GetStoneAmount() != 0 && townhallRange)
                {
                    StartCoroutine(AddStone());
                    yield break;
                }

                coroutineStarted = false;
            }
        }
        else if (gameObject.CompareTag("Archer"))
        {
            Archer archer = gameObject.GetComponent<Archer>();
            // wait three seconds
            yield return new WaitForSeconds(3);
            // remove a wood from the villager's inventory
            archer.RemoveWood(1);
            // add 5 wood to the townhalll
            playerOne.SetWood(playerOne.GetWood() + 5);
            // the villager still has wood and is in the townhall range
            if (archer.GetWoodAmount() != 0 && townhallRange)
            {
                StartCoroutine(AddWood());
            }
            else
            {
                // the villager has stone and is still in the townhall range
                if (archer.GetStoneAmount() != 0 && townhallRange)
                {
                    StartCoroutine(AddStone());
                    yield break;
                }
                coroutineStarted = false;
            }
        }
        else if (gameObject.CompareTag("Warrior"))
        {
            Warrior warrior = gameObject.GetComponent<Warrior>();
            // wait three seconds
            yield return new WaitForSeconds(3);
            // remove a wood from the villager's inventory
            warrior.RemoveWood(1);
            // add 5 wood to the townhall
            playerOne.SetWood(playerOne.GetWood() + 5);
            // the villager still has wood and is in the townhall range
            if (warrior.GetWoodAmount() != 0 && townhallRange)
            {
                StartCoroutine(AddWood());
            }
            else
            {
                // the villager has stone and is still in the townhall range
                if (warrior.GetStoneAmount() != 0 && townhallRange)
                {
                    StartCoroutine(AddStone());
                    yield break;
                }
                coroutineStarted = false;
            }
        }
    }

    public IEnumerator AddStone()
    {
        coroutineStarted = true;
        Player playerOne = player.GetPlayerOne();
        if (gameObject.CompareTag("Villager"))
        {
            Villager villager = gameObject.GetComponent<Villager>();
            // wait four seconds
            yield return new WaitForSeconds(4);
            villager.RemoveStone(1);
            playerOne.SetStone(playerOne.GetStone() + 5);
            if (villager.GetStoneAmount() != 0 && townhallRange)
            {
                StartCoroutine(AddStone());
            }
            else
            {
                coroutineStarted = false;
            }
        }
        else if (gameObject.CompareTag("Archer"))
        {
            Archer archer = gameObject.GetComponent<Archer>();
            // wait four seconds
            yield return new WaitForSeconds(4);
            archer.RemoveStone(1);
            playerOne.SetStone(playerOne.GetStone() + 5);
            if (archer.GetStoneAmount() != 0 && townhallRange)
            {
                StartCoroutine(AddStone());
            }
            else
            {
                coroutineStarted = false;
            }
        }
        else if (gameObject.CompareTag("Warrior"))
        {
            Warrior warrior = gameObject.GetComponent<Warrior>();
            // wait four seconds
            yield return new WaitForSeconds(4);
            warrior.RemoveStone(1);
            playerOne.SetStone(playerOne.GetStone() + 5);
            if (warrior.GetStoneAmount() != 0 && townhallRange)
            {
                StartCoroutine(AddStone());
            }
            else
            {
                coroutineStarted = false;
            }
        }
    }
}
