using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ObjectSelection : MonoBehaviour
{
    /*
     * Create lists for all the different objects that will be selectable by the player
     * Eventually I will make classes for all the different controllable characters in the game,
     * they will be instantiated into the game through some sort of baracks system
     *
     * For now there will be one list and this script will be used for testing
     *
     * Main idea for selection
     * Get the initial point, mouse 1 down. get the end point, mouse 1 up.
     *
     * For mouse 1 up, add 5 to the y value of the Vector3 so that
     *
     * Get the half way point between the two Vector3 points, instantiate a SelectionBox object
     * Z being the absolute value of the z coordinates subtracted by each other
     * X being the absolute value of the x coordinates subtracted by each oher
     * Y being 10
     * 
     * Instantiate a trigger hitbox that will take up the area created by those two vector three points
     */
    // Start is called before the first frame update
    public Texture selectionTexture;
    public Camera cam;
    
    private SelectionBox _selectionBox;
    // vars for BoxCollider creation
    private Vector3 startPoint;
    private Vector3 endPoint;
    // selection box UI vars
    private Vector2 UIBoxStartPos;
    private Vector2 UIBoxEndPos;
    
    [SerializeField]
    private String objTag = "";
    private bool raycastsBlocked = false;

    public GameObject[] characterPrefabs;
    public GameObject giveVillagersSign;

    private bool attemptingBarracksBuy = false;
    private int attemptedBuy;
    private GameObject currBarracks;
    
    // List of GameObjects selected
    [SerializeField] private List<GameObject> gameObjectsList;
    

    public UIScript userInterfaceController;
    public PlayerController player;
    public ObjectPlacer objectPlacer;

    private void Awake()
    {
        Vector3 startPoint = Vector3.zero;
        Vector3 endPoint = Vector3.zero;
        Vector2 UIBoxStartPos = Vector2.zero;
        Vector2 UIBoxEndPos = Vector2.zero;
        
        gameObjectsList = new List<GameObject>();
    }

    void Update()
    {
        // the raycast is not blocked and the game is not paused, then objects can be selected
        // coroutines will not be run during timeScale 0, so I would have to change up my selection method
        if (!raycastsBlocked && Time.timeScale != 0)
        {
            InstantiateSelectionBox();
            SelectionBoxRenderer();
        }
    }

    void InstantiateSelectionBox()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // if mouse button 0 is clicked again and the cursor is over the ground the List holding
            // the gameobjects should be erased the menu for the selected objects should also be left
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            // check if the cursor is over a UI object
            if (Physics.Raycast(ray, out hit))
            {
                if (gameObjectsList.Count != 0)
                {
                    RemoveSelection();
                }

                if (objectPlacer.GetObjectFinalized())
                {
                    startPoint = hit.point;
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            // reset the values of the UI Box selector
            UIBoxEndPos = UIBoxStartPos = Vector2.zero;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                // make the vector3 5 units higher than the click actually was
                endPoint = hit.point;
            }
        }

        if (startPoint != Vector3.zero && endPoint != Vector3.zero && startPoint != new Vector3(0,0,0) && endPoint != new Vector3(0,0,0))
        {
            // get the middle of the two points
            Vector3 middle = Vector3.Lerp(startPoint, endPoint, .5f);
            float widthX = Mathf.Abs(startPoint.x - endPoint.x);
            float widthZ = Mathf.Abs(startPoint.z - endPoint.z);
            _selectionBox = new SelectionBox(middle, widthX, widthZ, 10f);
            // reset startPoint and endPoint so infinite BoxColliders are not created
            ZeroBoxCorners();

            // Influence the list that was created from the BoxCollider
            StartCoroutine(WaitFrames("InfluenceSelection"));

            // start a coroutine to remove the SelectionBox over .1f seconds
            if (_selectionBox != null)
            {
                StartCoroutine(RemoveSelectionBox());
            }
        }
    }

    public void ZeroBoxCorners()
    {
        startPoint = Vector3.zero;
        endPoint = Vector3.zero;
    }

    private void InfluenceSelection()
    {
        if (gameObjectsList.Count != 0)
        {
            // find the most selected game object
            PrevalantTag(out objTag);
            // remove all the GameObjects from the list that don't have the correct tag
            for (int i = gameObjectsList.Count - 1; i >= 0; i--)
            {
                if (!gameObjectsList[i].CompareTag(objTag))
                {
                    gameObjectsList.Remove(gameObjectsList[i]); // remove all entries with incorrect tag
                }
            }
            
            foreach (GameObject obj in gameObjectsList)
            {
                if (objTag.Equals("Villager"))
                {
                    AudioController.PlayRandomGreeting();
                    // enable the selection circle
                    obj.transform.GetChild(3).gameObject.SetActive(true);
                    
                    // deactivate the menus if the player is not trying to buy something in the barracks
                    if (!attemptingBarracksBuy)
                    {
                        UIScript.DisableMainMenu();
                        UIScript.DisableAllStoreMenus();
                        UIScript.DisableAllEditMenus();
                    }
                    // allows player to move the character
                    obj.GetComponent<Villager>().SetSelected(true);
                    // enable a specific menu if only one character is selected
                    if (gameObjectsList.Count == 1 && !attemptingBarracksBuy)
                    {
                        UIScript.EnableEditMenu(2);
                        GameObject menu = UIScript.GetEditMenuList()[2];
                        InputField(0);
                        HealthSlider(0);
                    }
                }
                else if (objTag.Equals("Warrior"))
                {
                    AudioController.PlayRandomGreeting();
                    // enable the selection circle
                    obj.transform.GetChild(1).gameObject.SetActive(true);
                    
                    // deactivate the menus if the player is not trying to buy something in the barracks
                    if (!attemptingBarracksBuy)
                    {
                        UIScript.DisableMainMenu();
                        UIScript.DisableAllStoreMenus();
                        UIScript.DisableAllEditMenus();
                    }
                    // allows player to move the character
                    obj.GetComponent<Warrior>().SetSelected(true);
                    // enable a specific menu if only one character is selected
                    if (gameObjectsList.Count == 1 && !attemptingBarracksBuy)
                    {
                        UIScript.EnableEditMenu(2);
                        GameObject menu = UIScript.GetEditMenuList()[2];
                        InputField(2);
                        HealthSlider(2);
                    }
                }
                else if (objTag.Equals("Archer"))
                {
                    AudioController.PlayRandomGreeting();
                    // enable the selection circle
                    obj.transform.GetChild(1).gameObject.SetActive(true);
                    
                    // deactivate the menus if the player is not trying to buy something in the barracks
                    if (!attemptingBarracksBuy)
                    {
                        UIScript.DisableMainMenu();
                        UIScript.DisableAllStoreMenus();
                        UIScript.DisableAllEditMenus();
                    }
                    // allows player to move the character
                    obj.GetComponent<Archer>().SetSelected(true);
                    // enable a specific menu if only one character is selected
                    if (gameObjectsList.Count == 1 && !attemptingBarracksBuy)
                    {
                        UIScript.EnableEditMenu(2);
                        GameObject menu = UIScript.GetEditMenuList()[2];
                        InputField(1);
                        HealthSlider(1);
                    }
                }
                else if (objTag.Equals("HouseOne") && !attemptingBarracksBuy)
                {
                    // enable the selection square
                    obj.transform.GetChild(1).gameObject.SetActive(true);
                    
                    // deactivate the main menu
                    UIScript.DisableMainMenu();
                    // deactivate the store menu
                    UIScript.DisableStoreMenu(0);
                    // Open up HouseOne menu, index 0
                    UIScript.EnableEditMenu(0);
                } else if (objTag.Equals("Barracks") && !attemptingBarracksBuy)
                {
                    // enable the selection square
                    obj.transform.GetChild(1).gameObject.SetActive(true);
                    
                    // deactivate the main menu
                    UIScript.DisableMainMenu();
                    // deactivate the store menu
                    UIScript.DisableStoreMenu(1);
                    // Open up Barracks menu, index 1
                    UIScript.EnableEditMenu(1);

                    if (gameObjectsList.Count == 1)
                    {
                        UIScript.EnableArcherWarriorButtons();
                    }
                }
            }
        }
    }

    private void SubmitName(string name)
    {
        GameObject menu = UIScript.GetEditMenuList()[2];
        Transform nameText = menu.transform.GetChild(0);
        Transform inputField = menu.transform.GetChild(1);
        
        if (gameObjectsList[0].CompareTag("Villager"))
        {
            Villager villager = gameObjectsList[0].GetComponent<Villager>();
            villager.SetName(name);
            villager.LockName();
            inputField.gameObject.SetActive(false);
            inputField.GetComponent<TMP_InputField>().text = "";
            nameText.GetComponent<TMP_Text>().text = "Name: " + villager.GetName();
        }
        else if (gameObjectsList[0].CompareTag("Archer"))
        {
            Archer archer = gameObjectsList[0].GetComponent<Archer>();
            archer.SetName(name);
            archer.LockName();
            inputField.gameObject.SetActive(false);
            inputField.GetComponent<TMP_InputField>().text = "";
            nameText.GetComponent<TMP_Text>().text = "Name: " + archer.GetName();
        }
        else if (gameObjectsList[0].CompareTag("Warrior"))
        {
            Warrior warrior = gameObjectsList[0].GetComponent<Warrior>();
            warrior.SetName(name);
            warrior.LockName();
            inputField.gameObject.SetActive(false);
            inputField.GetComponent<TMP_InputField>().text = "";
            nameText.GetComponent<TMP_Text>().text = "Name: " + warrior.GetName();
        }
    }

    private void InputField(int type)
    {
        GameObject menu = UIScript.GetEditMenuList()[2];
        Transform characterName = menu.transform.GetChild(0);
        Transform inputField = menu.transform.GetChild(1);
        Transform textArea = inputField.GetChild(0);
        TMP_Text[] textChildren = textArea.GetComponentsInChildren<TMP_Text>();
        TMP_Text placeholderText = textChildren[0];

        if (type ==  0)
        {
            Villager villager = gameObjectsList[0].GetComponent<Villager>();
            if (!villager.GetNameSet()) // the villager's name hasn't been locked in yet
            {
                inputField.gameObject.SetActive(true);
                placeholderText.text = villager.GetName();
                var nameInput = inputField.GetComponent<TMP_InputField>();
                var nameListener = new TMP_InputField.SubmitEvent();
                nameListener.AddListener(SubmitName);
                nameInput.onEndEdit = nameListener;
            }
            else // the villagers name has been locked in
            {
                characterName.GetComponent<TMP_Text>().text = "Name: " + villager.GetName();
            }
        }
        // archer
        else if (type == 1)
        {
            Archer archer = gameObjectsList[0].GetComponent<Archer>();
            if (!archer.GetNameSet()) // the villager's name hasn't been locked in yet
            {
                inputField.gameObject.SetActive(true);
                placeholderText.text = archer.GetName();
                var nameInput = inputField.GetComponent<TMP_InputField>();
                var nameListener = new TMP_InputField.SubmitEvent();
                nameListener.AddListener(SubmitName);
                nameInput.onEndEdit = nameListener;
            }
            else // the villagers name has been locked in
            {
                characterName.GetComponent<TMP_Text>().text = "Name: " + archer.GetName();
            }
        }
        // warrior
        else if (type == 2)
        {
            Warrior warrior = gameObjectsList[0].GetComponent<Warrior>();
            if (!warrior.GetNameSet()) // the villager's name hasn't been locked in yet
            {
                inputField.gameObject.SetActive(true);
                placeholderText.text = warrior.GetName();
                var nameInput = inputField.GetComponent<TMP_InputField>();
                var nameListener = new TMP_InputField.SubmitEvent();
                nameListener.AddListener(SubmitName);
                nameInput.onEndEdit = nameListener;
            }
            else // the villagers name has been locked in
            {
                characterName.GetComponent<TMP_Text>().text = "Name: " + warrior.GetName();
            }
        }
    }

    private void HealthSlider(int type)
    {
        GameObject menu = UIScript.GetEditMenuList()[2];
        Transform slider = menu.transform.GetChild(2);
        TMP_Text healthText = slider.GetComponentInChildren<TMP_Text>();
        if (type == 0)
        {
            Villager villager = gameObjectsList[0].GetComponent<Villager>();
            healthText.text = villager.GetHealth() + "/" + villager.GetMaxHealth();
            slider.GetComponent<Slider>().maxValue = villager.GetMaxHealth();
            slider.GetComponent<Slider>().value = villager.GetHealth();
        }
        // archer
        else if (type == 1)
        {
            Archer archer = gameObjectsList[0].GetComponent<Archer>();
            healthText.text = archer.GetHealth() + "/" + archer.GetMaxHealth();
            slider.GetComponent<Slider>().maxValue = archer.GetMaxHealth();
            slider.GetComponent<Slider>().value = archer.GetHealth();
        }
        // warrior
        else if (type == 2)
        {
            Warrior warrior = gameObjectsList[0].GetComponent<Warrior>();
            healthText.text = warrior.GetHealth() + "/" + warrior.GetMaxHealth();
            slider.GetComponent<Slider>().maxValue = warrior.GetMaxHealth();
            slider.GetComponent<Slider>().value = warrior.GetHealth();
        }
    }
    
    // this method will only be called if one barracks is selected
    public void AttemptBuyAtBarracks(int a)
    {
        Player playerOne = player.GetPlayerOne();
        // check if the player has enough resources to buy the character upgrade
        if (a == 0 && !attemptingBarracksBuy)
        {
            if (playerOne.GetGold() >= 50 && playerOne.GetWood() >= 15)
            {
                playerOne.SetGold(playerOne.GetGold() - 50);
                playerOne.SetWood(playerOne.GetWood() - 15);
            }
            else
            {
                return;
            }   
        }
        else if (a == 1 && !attemptingBarracksBuy)
        {
            if (playerOne.GetGold() >= 100 && playerOne.GetStone() >= 5 && playerOne.GetWood() >= 10)
            {
                playerOne.SetGold(playerOne.GetGold() - 100);
                playerOne.SetWood(playerOne.GetWood() - 10);
                playerOne.SetStone(playerOne.GetStone() - 5);
            }
            else
            {
                return;
            }
        }

        if (!attemptingBarracksBuy)
        {
            attemptingBarracksBuy = true;
            // in any case we want to enable the sign and bring it to the position of the barracks
            currBarracks = gameObjectsList[0];
            giveVillagersSign.transform.position = currBarracks.transform.position + new Vector3(0, 20, 0);
            giveVillagersSign.SetActive(true);
            currBarracks.transform.GetChild(2).gameObject.SetActive(true);
            attemptedBuy = a;
        }
    }

    public void StartVillagerTraining()
    {
        // the player attempted to buy an archer
        if (attemptedBuy == 0)
        {
            player.InstantiateArcher(currBarracks.transform.position + new Vector3(0, 0, 20));
            attemptingBarracksBuy = false;
        }
        // the player attempted to buy a warrior
        else if (attemptedBuy == 1)
        {
            player.InstantiateWarrior(currBarracks.transform.position + new Vector3(0, 0, 20));
            attemptingBarracksBuy = false;
        }
        currBarracks.transform.GetChild(1).gameObject.SetActive(false);
    }
    public void CancelBarracksBuy()
    {
        if (attemptingBarracksBuy)
        {
            Player playerOne = player.GetPlayerOne();
            if (attemptedBuy == 0)
            {
                playerOne.SetGold(playerOne.GetGold() + 50);
                playerOne.SetWood(playerOne.GetWood() + 15);
            }
            else if (attemptedBuy == 1)
            {
                playerOne.SetGold(playerOne.GetGold() + 100);
                playerOne.SetWood(playerOne.GetWood() + 10);
                playerOne.SetStone(playerOne.GetStone() + 5);
            }
            giveVillagersSign.SetActive(false);
            attemptingBarracksBuy = false;
        }
        // disable the selection square
        currBarracks.transform.GetChild(1).gameObject.SetActive(false);
        currBarracks.transform.GetChild(2).gameObject.SetActive(false);
        // Close the Barracks menu, index 1
        UIScript.DisableEditMenu(1);
        // attempt to disable the archer and warrior buttons
        UIScript.DisableArcherWarriorButtons();
        // set the main menu to active
        UIScript.EnableMainMenu();
    }

    private void PrevalantTag(out String objTag)
    {
        objTag = "";
        List<String> tagList = new List<string>();
        List<int> tagAmounts = new List<int>();
        foreach (GameObject entry in gameObjectsList)
        {
            String currTag = entry.tag; 
            if (!tagList.Contains(currTag)) // check if the tag of the current object is in 
            {
                tagList.Add(currTag); // add that tag to the tag list
                tagAmounts.Add(0); // add a base of 0 for that tag in a parallel list
            }
        }
        // for loop with int i since I need to index for the parallel list
        for (int i = 0; i < tagList.Count; i++)
        {
            foreach (GameObject entry in gameObjectsList)
            {
                String currTag = entry.tag;
                if (tagList[i].Equals(currTag))
                {
                    tagAmounts[i]++;
                }
            }
        }
        // find the tag with the most repetition
        int maxAmount = 0;
        for (int i = 0; i < tagList.Count; i++)
        {
            // if the same amount of objects are selected, then the object selected first will be defaulted to
            if (tagAmounts[i] > maxAmount)
            {
                maxAmount = tagAmounts[i]; // update the maxAmount
                objTag = tagList[i]; // update the output String
            }
        }
    }

    public void SelectionBoxRenderer()
    {
        if (Input.GetMouseButton(0))
        {
            // credit for box UI df424
            // Called on the first update where the user has pressed the mouse button.
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                UIBoxStartPos = Input.mousePosition;
            }
            else // Else we must be in "drag" mode.
            {
                UIBoxEndPos = Input.mousePosition;
            }
        }
    }

    private void OnGUI()
    {
        // If we are in the middle of a selection draw the texture.
        if (UIBoxStartPos != Vector2.zero && UIBoxEndPos != Vector2.zero)
        {
            GUI.DrawTexture(new Rect(UIBoxStartPos.x, Screen.height - UIBoxStartPos.y, // position
                    UIBoxEndPos.x - UIBoxStartPos.x, // width
                    -1 * ((Screen.height - UIBoxStartPos.y) - (Screen.height - UIBoxEndPos.y))), // height
                selectionTexture); // texture
        }
        
    }

    public void RemoveSelection()
    {
        if (gameObjectsList.Count != 0)
        {
            foreach (GameObject obj in gameObjectsList)
            {
                if (objTag.Equals("Villager"))
                {
                    // disable the selection circle
                    obj.transform.GetChild(3).gameObject.SetActive(false);
                    // allows the player to update the movement position of the character
                    obj.GetComponent<Villager>().SetSelected(false);
                    UIScript.DisableEditMenu(2);
                    // turns off the villager input field even if nothing has been inputted
                    UIScript.DisableVillagerInputField();
                    // set the main menu to active
                    UIScript.EnableMainMenu();
                } 
                else if (objTag.Equals("Archer"))
                {
                    // disable the selection circle
                    obj.transform.GetChild(1).gameObject.SetActive(false);
                    // allows the player to update the movement position of the character
                    obj.GetComponent<Archer>().SetSelected(false);
                    UIScript.DisableEditMenu(2);
                    // turns off the villager input field even if nothing has been inputted
                    UIScript.DisableVillagerInputField();
                    // set the main menu to active
                    UIScript.EnableMainMenu();
                }
                else if (objTag.Equals("Warrior"))
                {
                    // disable the selection circle
                    obj.transform.GetChild(1).gameObject.SetActive(false);
                    // allows the player to update the movement position of the character
                    obj.GetComponent<Warrior>().SetSelected(false);
                    UIScript.DisableEditMenu(2);
                    // turns off the villager input field even if nothing has been inputted
                    UIScript.DisableVillagerInputField();
                    // set the main menu to active
                    UIScript.EnableMainMenu();
                }
                else if (objTag.Equals("HouseOne"))
                {
                    // disable the selection square
                    obj.transform.GetChild(1).gameObject.SetActive(false);
                    // Close the HouseOne menu, index 0
                    UIScript.DisableEditMenu(0);
                    // set the main menu to active
                    UIScript.EnableMainMenu();
                } else if (objTag.Equals("Barracks") && !attemptingBarracksBuy)
                {
                    // disable the selection square
                    obj.transform.GetChild(1).gameObject.SetActive(false);
                    gameObjectsList[0].transform.GetChild(2).gameObject.SetActive(false);
                    // Close the Barracks menu, index 1
                    UIScript.DisableEditMenu(1);
                    // attempt to disable the archer and warrior buttons
                    UIScript.DisableArcherWarriorButtons();
                    giveVillagersSign.SetActive(false);
                    // set the main menu to active
                    UIScript.EnableMainMenu();
                }
            }
        }
        gameObjectsList = new List<GameObject>();
    }

    public void RemoveHouseOne()
    {
        for (int i = gameObjectsList.Count - 1; i >= 0; i--)
        {
            // remove the game object
            player.RemoveHouseOne(gameObjectsList[i]);
            gameObjectsList.Remove(gameObjectsList[i]);
        }
        // Close the HouseOne menu, index 0
        UIScript.DisableEditMenu(0);
        // set the main menu to active
        UIScript.EnableMainMenu();
    }

    public void RemoveBarracks()
    {
        // Close the Barracks menu, index 1
        UIScript.DisableEditMenu(1);
        // disable the archer warrior buy buttons
        UIScript.DisableArcherWarriorButtons();
        // set the main menu to active
        UIScript.EnableMainMenu();
        if (attemptingBarracksBuy)
        {
            Player playerOne = player.GetPlayerOne();
            if (attemptedBuy == 0)
            {
                playerOne.SetGold(playerOne.GetGold() + 50);
                playerOne.SetWood(playerOne.GetWood() + 15);
            }
            else if (attemptedBuy == 1)
            {
                playerOne.SetGold(playerOne.GetGold() + 100);
                playerOne.SetWood(playerOne.GetWood() + 10);
                playerOne.SetStone(playerOne.GetStone() + 5);
            }
            giveVillagersSign.SetActive(false);
            attemptingBarracksBuy = false;
        }
        for (int i = gameObjectsList.Count - 1; i >= 0; i--)
        {
            player.RemoveBarracks(gameObjectsList[i]);
            gameObjectsList.Remove(gameObjectsList[i]);
        }
    }

    IEnumerator RemoveSelectionBox()
    {
        yield return new WaitForSeconds(.1f);
        Destroy(_selectionBox.hitboxObj);
    }

    IEnumerator WaitFrames(string method)
    {
        yield return new WaitForSeconds(.1f);
        if (method.Equals("InfluenceSelection"))
        {
            InfluenceSelection();
        }
    }

    public void BlockRaycasts(bool a)
    {
        raycastsBlocked = a;
    }

    public void AddGameObject(GameObject a)
    {
        gameObjectsList.Add(a);
    }

    public List<GameObject> GetSelectionList()
    {
        return gameObjectsList;
    }

    public void ChangeActiveStatusGiveVillagerSign(bool a)
    {
        giveVillagersSign.SetActive(a);
    }
}
