using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Archer : MonoBehaviour
{
    // public PlayerController player;
    // public Grid grid;

    public NavMeshAgent archerAgent;
    public Animator archerAnimator;

    private GameObject proceduralEnemySpawner;
    
    // object placement variable
    [SerializeField] private Vector3 currPosition;
    // Movement variables
    private Vector3 targetPosition;
    private Vector3 lookAtTarget;
    private Quaternion playerRot;
    private bool selected = false;
    private bool nameSet = false;
    // private variables for villager interaction
    private int maxHealth;
    private int health;
    private int speed;
    private int damage;
    private int rotSpeed;
    private int chopDamage;
    private int mineSpeed;
    [SerializeField] private int wood;
    private int maxWood;
    private int stone;
    private int maxStone;
    // booleans related to animation
    private bool moving = false;
    private bool attacking = false;
    private bool idle = true;

    // villager count
    private static int archerCount = 1;
    // villager attributes
    private string name;
    private Coroutine deathCoroutine;

    private void Awake()
    {
        proceduralEnemySpawner = GameObject.Find("ProceduralEnemySpawner");
        currPosition = new Vector3(0f, -600f, 0f);
        maxHealth = 75;
        health = maxHealth;
        speed = 5;
        damage = 10;
        rotSpeed = 5;
        chopDamage = 10;
        mineSpeed = 10;
        maxWood = 3;
        maxStone = 3;
        wood = 0;
        stone = 0;
        name = "Archer " + archerCount;
        archerCount++;
    }

    private void Update()
    {
        if (Input.GetMouseButton(1) && selected)
        { 
            SetTargetPosition();
        }

        // destroy the villager if the health is less than or equal to zero
        if (health <= 0)
        {
            archerAnimator.SetBool("dead", true);
            if (deathCoroutine == null)
            {
                AudioController.PlayYodellingAndFalling();
                deathCoroutine = StartCoroutine(nameof(DestroyCharacterObject));
            }
        }

        if (archerAgent.hasPath)
        {
            if (Math.Abs(transform.position.magnitude - archerAgent.pathEndPosition.magnitude) < .1f)
            {
                moving = false;
                idle = true;
            }
            else
            {
                moving = true;
                idle = false;   
            }
        }
        
        archerAnimator.SetBool("moving",  moving);
        archerAnimator.SetBool("attacking", attacking);
        archerAnimator.SetBool("idle", idle);

    }
    
    void SetTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.CompareTag("Ground"))
            {
                archerAgent.SetDestination(hit.point);
            }
        }
    }

    private IEnumerator DestroyCharacterObject()
    {
        // remove the selection of the villager so that information from the villager is not trying to be accessed
        // after it is destroyed
        GameObject.Find("SelectionController").GetComponent<ObjectSelection>().RemoveSelection();
        // remove the reference of the villager from playerOne in the playerController
        GameObject.Find("PlayerController").GetComponent<PlayerController>().GetPlayerOne().RemoveVillager(gameObject);
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }

    // setters
    public void SetSelected(bool selected)
    {
        this.selected = selected;
    }
    
    public void SetHealth(int health)
    {
        this.health = health;
    }

    public void SetSpeed(int speed)
    {
        this.speed = speed;
    }

    public void SetDamage(int damage)
    {
        this.damage = damage;
    }

    public void RemoveHealth(int damageDealt)
    {
        health -= damageDealt;
        GameObject singleCharacterMenu = GameObject.Find("SingleCharacter");
        if (singleCharacterMenu != null && selected)
        {
            singleCharacterMenu.transform.GetChild(2).GetComponent<Slider>().value = health;
            singleCharacterMenu.transform.GetChild(2).GetChild(2).GetComponent<TextMeshProUGUI>().text =
                health + "/" + maxHealth;
        }
    }

    public void LockName()
    {
        nameSet = true;
    }

    public void SetName(String a)
    {
        name = a;
    }

    public void AddWood(int a)
    {
        wood += a;
    }

    public void AddStone(int a)
    {
        stone += a;
    }

    public void RemoveWood(int a)
    {
        wood -= a;
    }

    public void RemoveStone(int a)
    {
        stone -= a;
    }

    public void SetAttacking(bool a)
    {
        attacking = a;
    }

    // getters
    public int GetHealth()
    {
        return health;
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }
    
    public int GetSpeed()
    {
        return speed;
    }

    public int GetDamage()
    {
        return damage;
    }

    public int GetChopDamage()
    {
        return chopDamage;
    }

    public int GetMineSpeed()
    {
        return mineSpeed;
    }

    public String GetName()
    {
        return name;
    }

    public bool GetNameSet()
    {
        return nameSet;
    }

    public int GetWoodAmount()
    {
        return wood;
    }

    public int GetStoneAmount()
    {
        return stone;
    }

    public bool GetWoodInventory()
    {
        // if the inventory is full return false
        if (wood >= maxWood)
        {
            return false;
        }
        return true;
    }

    public bool GetStoneInventory()
    {
        // if the inventory is full return false
        if (stone >= maxStone)
        {
            return false;
        }
        return true;
    }
    
}

