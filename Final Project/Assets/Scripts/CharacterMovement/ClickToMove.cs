using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class ClickToMove : MonoBehaviour
{
    private Vector3 targetPosition;
    private Vector3 lookAtTarget;
    private Quaternion playerRot;
    private bool moving = false;
    private bool selected = false;
    
    public float rotSpeed = 5f;
    public float speed = 5f;

    // Update is called once per frame
    void Update()
    {
        // if the user right clicks the ground the character selected will move to that location
        if (Input.GetMouseButton(1) && selected)
        { 
            SetTargetPosition();
        }
        if (moving)
            Move();
    }

    void SetTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit))
        {
            targetPosition = hit.point;
            //transform.LookAt(targetPosition);
            // calculate the angle to new position
            lookAtTarget = new Vector3(targetPosition.x - transform.position.x, transform.position.y, targetPosition.z - transform.position.z);
            playerRot = Quaternion.LookRotation(lookAtTarget);
            moving = true; // allow character to start moving
        }
    }

    void Move()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, playerRot, Time.deltaTime * rotSpeed);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * speed);
        if (transform.position.magnitude - targetPosition.magnitude < .3f && transform.position.magnitude - targetPosition.magnitude > 0)
            moving = false; // stop character from moving
    }

    public void SetSelected(bool selected)
    {
        this.selected = selected;
    }
}
