using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogPickUp : MonoBehaviour
{
    private bool characterCollecting = false;

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Villager") && !characterCollecting)
        {
            characterCollecting = true;
            Villager villager = other.gameObject.GetComponent<Villager>();
            // if the villager's inventory isn't full add one wood to the villager
            if (villager.GetWoodInventory())
            {
                villager.AddWood(1);
                Destroy(gameObject.transform.parent.gameObject);
            }
            else
            {
                characterCollecting = false;
            }
        }
        if (other.CompareTag("Archer") && !characterCollecting)
        {
            characterCollecting = true;
            Archer archer = other.gameObject.GetComponent<Archer>();
            // if the villager's inventory isn't full add one wood to the villager
            if (archer.GetWoodInventory())
            {
                archer.AddWood(1);
                Destroy(gameObject.transform.parent.gameObject);
            }
            else
            {
                characterCollecting = false;
            }
        }
        if (other.CompareTag("Warrior") && !characterCollecting)
        {
            characterCollecting = true;
            Warrior warrior = other.gameObject.GetComponent<Warrior>();
            // if the villager's inventory isn't full add one wood to the villager
            if (warrior.GetWoodInventory())
            {
                warrior.AddWood(1);
                Destroy(gameObject.transform.parent.gameObject);
            }
            else
            {
                characterCollecting = false;
            }
        }
    }
}
