using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;
using DG.Tweening;

public class OakTree : MonoBehaviour
{
    public Animator oakTreeAnimator;
    public GameObject[] logPrefabs;
    public Transform[] spawnPositions;

    // main private variables - health, maxHealth -
    private int maxHealth;
    [SerializeField] private int health;
    private bool treeChopped;
    private bool characterPresent;
    private bool coroutineStarted = false;

    // variables used to damage the tree
    private GameObject currCharacter;
    private Slider slider;
    
    // Start is called before the first frame update
    void Start()
    {
        maxHealth = 50;
        health = maxHealth;
        slider = transform.GetChild(0).GetChild(0).GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0 && !coroutineStarted)
        {
            // Set treeChopped to true
            treeChopped = true;
            // remove the trigger BoxCollider from the OakTree gameObject
            Destroy(gameObject.GetComponent<BoxCollider>());
            // deactivate the slider
            transform.GetChild(1).gameObject.SetActive(false);
            // after 3 seconds tween the GameObject down 5 units y and destroy the game object
            StartCoroutine(nameof(DestroyTree));
        }

        if (!characterPresent)
        {
            StopCoroutine(nameof(ChopTree));
        }
        
        oakTreeAnimator.SetBool("TreeChopped", treeChopped);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Villager") && !characterPresent)
        {
            characterPresent = true;
            currCharacter = other.gameObject;
            // activate the slider
            slider.maxValue = maxHealth;
            slider.value = health;
            transform.GetChild(0).gameObject.SetActive(true);
            StartCoroutine(nameof(ChopTree));
        }
        if (other.CompareTag("Warrior") && !characterPresent)
        {
            characterPresent = true;
            currCharacter = other.gameObject;
            // activate the slider
            slider.maxValue = maxHealth;
            slider.value = health;
            transform.GetChild(0).gameObject.SetActive(true);
            StartCoroutine(nameof(ChopTree));
        }
        if (other.CompareTag("Archer") && !characterPresent)
        {
            characterPresent = true;
            currCharacter = other.gameObject;
            // activate the slider
            slider.maxValue = maxHealth;
            slider.value = health;
            transform.GetChild(0).gameObject.SetActive(true);
            StartCoroutine(nameof(ChopTree));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // deactivate the slider
        transform.GetChild(0).gameObject.SetActive(false);
        characterPresent = false;
    }

    IEnumerator ChopTree()
    {
        yield return new WaitForSeconds(1);
        // decrement the health depending on the chopDamage of the villager
        if (currCharacter.CompareTag("Villager"))
        {
            // update the health of the tree
            health -= currCharacter.GetComponent<Villager>().GetChopDamage();
            // update the visual representation of the health
            slider.value = health;
            // if the character is still there and the health is above 0 continue the chopping
            if (characterPresent && health >= 0)
            {
                StartCoroutine(nameof(ChopTree));
            }
        }
        if (currCharacter.CompareTag("Archer"))
        {
            // update the health of the tree
            health -= currCharacter.GetComponent<Archer>().GetChopDamage();
            // update the visual representation of the health
            slider.value = health;
            // if the character is still there and the health is above 0 continue the chopping
            if (characterPresent && health >= 0)
            {
                StartCoroutine(nameof(ChopTree));
            }
        }
        if (currCharacter.CompareTag("Warrior"))
        {
            // update the health of the tree
            health -= currCharacter.GetComponent<Warrior>().GetChopDamage();
            // update the visual representation of the health
            slider.value = health;
            // if the character is still there and the health is above 0 continue the chopping
            if (characterPresent && health >= 0)
            {
                StartCoroutine(nameof(ChopTree));
            }
        }
    }

    IEnumerator DestroyTree()
    {
        coroutineStarted = true;
        Transform oldParent = spawnPositions[0].parent;
        spawnPositions[0].parent = null;
        spawnPositions[1].parent = null;
        spawnPositions[2].parent = null;
        yield return new WaitForSeconds(3);
        // remove BoxCollider from the tree
        Destroy(transform.GetChild(0).gameObject);
        // reset the transforms of the spawn points
        spawnPositions[0].parent = oldParent;
        spawnPositions[1].parent = oldParent;
        spawnPositions[2].parent = oldParent;
        // spawn the logs
        Instantiate(logPrefabs[0], spawnPositions[0].position, logPrefabs[0].transform.rotation);
        Instantiate(logPrefabs[1], spawnPositions[1].position, logPrefabs[1].transform.rotation);
        Instantiate(logPrefabs[2], spawnPositions[2].position, logPrefabs[2].transform.rotation);
        transform.DOMove(transform.position - new Vector3(0f, 5f, 0f), 6);
        yield return new WaitForSeconds(6.1f);
        Destroy(gameObject);
    }
}
