using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;
using DG.Tweening;

public class Rock : MonoBehaviour
{
    public GameObject[] rockPiecePrefabs;
    public Transform[] spawnPositions;

    // main private variables - health, maxHealth -
    private int maxHealth;
    [SerializeField] private int health;
    private bool characterPresent;
    private bool coroutineStarted = false;
    private int rockCounter;

    // variables used to damage the tree
    private GameObject currCharacter;
    private Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        rockCounter = 4;
        maxHealth = 100;
        health = maxHealth;
        slider = transform.GetChild(2).GetChild(0).GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0 && !coroutineStarted)
        {
            // remove the trigger BoxCollider from the OakTree gameObject
            Destroy(gameObject.GetComponent<BoxCollider>());
            // deactivate the slider
            transform.GetChild(2).gameObject.SetActive(false);
            // after 6 seconds tween the GameObject down 8 units y and destroy the game object
            StartCoroutine(nameof(DestroyRock));
        }
        
        if (!characterPresent)
        {
            StopCoroutine(nameof(MineRock));
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        // no matter what type of character, set the camera for the canvas
        gameObject.transform.GetChild(2).gameObject.GetComponent<Canvas>().worldCamera = Camera.current;
        if (other.CompareTag("Villager") && !characterPresent)
        {
            characterPresent = true;
            currCharacter = other.gameObject;
            // activate the slider
            slider.maxValue = maxHealth;
            slider.value = health;
            transform.GetChild(2).gameObject.SetActive(true);
            StartCoroutine("MineRock");
        }
        if (other.CompareTag("Archer") && !characterPresent)
        {
            characterPresent = true;
            currCharacter = other.gameObject;
            // activate the slider
            slider.maxValue = maxHealth;
            slider.value = health;
            transform.GetChild(2).gameObject.SetActive(true);
            StartCoroutine(nameof(MineRock));
        }
        if (other.CompareTag("Warrior") && !characterPresent)
        {
            characterPresent = true;
            currCharacter = other.gameObject;
            // activate the slider
            slider.maxValue = maxHealth;
            slider.value = health;
            transform.GetChild(2).gameObject.SetActive(true);
            StartCoroutine(nameof(MineRock));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // deactivate the slider
        transform.GetChild(2).gameObject.SetActive(false);
        characterPresent = false;
    }


    IEnumerator MineRock()
    {
        if (currCharacter.CompareTag("Villager"))
        {
            // create villager ref, main rock parent ref, and spawn point ref
            Transform rockParent = gameObject.transform.GetChild(0);
            Transform spawnPointParent = gameObject.transform.GetChild(1);
            Villager currVillager = currCharacter.GetComponent<Villager>();
            // wait for the amount of mine speed the current villager has
            yield return new WaitForSeconds(currVillager.GetMineSpeed());
            // there are still rock pieces to be instantiated
            if (rockCounter != 0)
            {
                // enable the next rock gameObject
                rockParent.GetChild(1).gameObject.SetActive(true);
                // destroy the current rock gameObject
                Destroy(rockParent.GetChild(0).gameObject);
                // Instantiate the rock piece prefab, then destroy that spawn point
                Instantiate(rockPiecePrefabs[rockCounter - 1], spawnPointParent.GetChild(0).position, Quaternion.identity);
                Destroy(spawnPointParent.GetChild(0).gameObject);
                // decrement the rockCounter
                rockCounter--;
                health -= 25;
                if (characterPresent)
                {
                    StartCoroutine("MineRock");
                }
            }
        }
        if (currCharacter.CompareTag("Archer"))
        {
            // create villager ref, main rock parent ref, and spawn point ref
            Transform rockParent = gameObject.transform.GetChild(0);
            Transform spawnPointParent = gameObject.transform.GetChild(1);
            Archer currArcher = currCharacter.GetComponent<Archer>();
            // wait for the amount of mine speed the current villager has
            yield return new WaitForSeconds(currArcher.GetMineSpeed());
            // there are still rock pieces to be instantiated
            if (rockCounter != 0)
            {
                // enable the next rock gameObject
                rockParent.GetChild(1).gameObject.SetActive(true);
                // destroy the current rock gameObject
                Destroy(rockParent.GetChild(0).gameObject);
                // Instantiate the rock piece prefab, then destroy that spawn point
                Instantiate(rockPiecePrefabs[rockCounter - 1], spawnPointParent.GetChild(0).position, Quaternion.identity);
                Destroy(spawnPointParent.GetChild(0).gameObject);
                // decrement the rockCounter
                rockCounter--;
                health -= 25;
                if (characterPresent)
                {
                    StartCoroutine(nameof(MineRock));
                }
            }
        }
        if (currCharacter.CompareTag("Warrior"))
        {
            // create villager ref, main rock parent ref, and spawn point ref
            Transform rockParent = gameObject.transform.GetChild(0);
            Transform spawnPointParent = gameObject.transform.GetChild(1);
            Warrior currVillager = currCharacter.GetComponent<Warrior>();
            // wait for the amount of mine speed the current villager has
            yield return new WaitForSeconds(currVillager.GetMineSpeed());
            // there are still rock pieces to be instantiated
            if (rockCounter != 0)
            {
                // enable the next rock gameObject
                rockParent.GetChild(1).gameObject.SetActive(true);
                // destroy the current rock gameObject
                Destroy(rockParent.GetChild(0).gameObject);
                // Instantiate the rock piece prefab, then destroy that spawn point
                Instantiate(rockPiecePrefabs[rockCounter - 1], spawnPointParent.GetChild(0).position, Quaternion.identity);
                Destroy(spawnPointParent.GetChild(0).gameObject);
                // decrement the rockCounter
                rockCounter--;
                health -= 25;
                if (characterPresent)
                {
                    StartCoroutine(nameof(MineRock));
                }
            }
        }
    }

    IEnumerator DestroyRock()
    {
        coroutineStarted = true;
        transform.DOMove(transform.position + new Vector3(0, -8, 0), 6);
        yield return new WaitForSeconds(6.1f);
        Destroy(gameObject);
    }
}
